$(function() {

	const mq = window.matchMedia( "(min-width: 768px)" );
	var address = 'youth';
	var steps = 0;
    var clickedElement;
    var clickable;

	function calculateHeight(){
		if (mq.matches) {
			// window width is at least 768px
			for (i = 1; i < 6; i++) {
			   switch(i) {
			       	case 1:
			       		var imgHeight = $( ".img-bg-1" ).height();
			       		var videoPlay = $( ".video-play" ).height();
           				$(".first-section").css('height', imgHeight);
           				$(".first-section").css('min-height', imgHeight);
           				$(".bg-first-section").css('height', videoPlay);
			           	break;
			       	case 2:
			           	var imgHeight = $( ".img-bg-2" ).height();
           				$(".second-section").css('height', imgHeight);
           				$(".second-section").css('min-height', imgHeight);
			          	break;
		        	case 3:
		            	var imgHeight = $( ".img-bg-3" ).height();
		            	$(".third-section").css('height', imgHeight);
		           		break;
   			       	case 4:
           				var imgHeight = $( ".img-bg-4" ).height();
           				$(".fourth-section").css('height', imgHeight);
   			          	break;
   		        	case 5:
   		            	var imgHeight = $( ".img-bg-5" ).height();
        				$(".fifth-section").css('height', imgHeight);
        				$(".fifth-section").css('min-height', imgHeight);
   		           		break;
			       	default:
			        	console.log(i)
			   }
			}
		} else {
			// window width is at less than 768px
			for (i = 1; i < 6; i++) {
			   switch(i) {
			       	case 1:
			       		var imgHeight = $( ".img-bg-1-mobile" ).height();
			       		$(".first-section").css('min-height', imgHeight);
			       		$(".first-section").css('height', 'initial');
			       		$(".bg-first-section").css('height', 'initial');
			           	break;
			       	case 2:
			           	var imgHeight = $( ".img-bg-2-mobile" ).height();
           			  	$(".second-section").css('min-height', imgHeight);
           			  	$(".second-section").css('height', 'initial');
			          	break;
		        	case 3:
		            	$(".third-section").css('height', 'initial');
		           		break;
   			       	case 4:
           				$(".fourth-section").css('height', 'initial');
   			          	break;
   		        	case 5:
   		            	var imgHeight = $( ".img-bg-5-mobile" ).height();
        			   	$(".fifth-section").css('min-height', imgHeight);
        			  	$(".fifth-section").css('height', 'initial');
   		           		break;
			       	default:
			        	console.log(i)
			   }
			}
		}
	}

	Pace.on('done', function(){
		calculateHeight();
	})

	var resizeTimeout;
	$( window ).resize(function() {
		if(!!resizeTimeout){ clearTimeout(resizeTimeout); }
	  	resizeTimeout = setTimeout(function(){
		    calculateHeight();
	  	},200);
	});

	var $root = $('html, body');
	$('.navbar-nav a, .arrow-section, .toTop').click(function() {
	    $root.animate({
	        scrollTop: $( $.attr(this, 'href') ).offset().top
	    }, 500);
	    return false;
	});

	(clickable = function() {
        $(".steps.clickable").on('click', function(e){
            clickedElement = e.currentTarget.attributes[1].nodeValue;
            $(this).addClass("active");
            launchLightBox();
            loadPlayer(e.currentTarget.attributes[1].nodeValue);
            $.ajax({
                url: '../users/steps',
                type: 'POST',
                data: { step:e.currentTarget.attributes[1].nodeValue}
            })
            .done(function() {
                if (e.currentTarget.attributes[1].nodeValue === "6"){
                    $(".steps-6").css("display", "none");
                    $(".cargar-cv").css("display", "block");
                }
            });
        });
    })();

    function getVideo(e) {
        if (e === "2"){
            return 'CZnvCpryQX8';
        } else if (e === '3'){
            return '_xEEOyehYZY';
        } else if (e === '4'){
            return 'bIx1Vez0l6k';
        } else if (e === '5'){
            return 'PihVBeLFzkg';
        } else if (e === '6'){
            return 'o72fc0K52fg';
        }
    }

    function getWidth(){
        if (mq.matches) {
            // window width is at least 768px
            return '768';
        } else {
            // window width is at less than 768px
            return '100%';
        }
    }

    function loadPlayer(e) {
        if (typeof(YT) == 'undefined' || typeof(YT.Player) == 'undefined') {
            var tag = document.createElement('script');
            tag.src = "https://www.youtube.com/iframe_api";
            var firstScriptTag = document.getElementsByTagName('script')[0];
            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
            window.onYouTubePlayerAPIReady = function() {
                onYouTubePlayer(e);
            };
        } else {
            onYouTubePlayer(e);
        }
    }

    var player;

    function onYouTubePlayer(e) {
        player = new YT.Player('player', {
            height: '490',
            width: getWidth(),
            videoId: getVideo(e),
            playerVars: { 'controls': 1, 'showinfo': 0, 'rel': 0, 'showsearch': 0, 'iv_load_policy': 3, 'autoplay': 1, 'modestbranding': 1, 'disablekb': 1 },
            events: {
                'onStateChange': onPlayerStateChange,
                'onError': catchError,
                'onReady': onPlayerReady
            }
        });
    }

    var done = false;

    function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.PLAYING && !done) {
            done = true;
        } else if (event.data == YT.PlayerState.ENDED) {
            instance.close().then(function() {});
        }
    }

    function onPlayerReady(event) {
        var embedCode = event.target.getVideoEmbedCode();
        event.target.playVideo();
        if (document.getElementById('embed-code')) {
        	document.getElementById('embed-code').innerHTML = embedCode;
        }
    }

    function catchError(event) {
        if (event.data == 100) console.log("Error");
    }

    function stopVideo() {
        player.stopVideo();
    }

    //Lightbox for videos
    function launchLightBox(){
        instance = lity('<div><div id="player"></div></div>');
        instance.options('esc', false);
    }

    $(document).on('lity:remove', function(event, instance) {
        switch(clickedElement){
            case "2":
                $(".steps-3").addClass("clickable");
                $(".download-reward-2").addClass("visible");
                $(".download-reward-2-1").addClass("visible");
                break;
            case "3":
                $(".steps-4").addClass("clickable");
                $(".download-reward-3").addClass("visible");
                $(".download-reward-3-1").addClass("visible");
                break;
            case "4":
                $(".steps-5").addClass("clickable");
                $(".download-reward-4").addClass("visible");
                break;
            case "5":
                $(".steps-6").addClass("clickable");
                break;
            case "6":
                $(".steps-6").css('display','none');
                $(".cargar-cv").css('display', 'block');
                break;
        }
        $(".clickable").off('click');
        clickable();
    });
});