$(function() {

	Pace.on('done', function(){
		setTimeout(function(){
			$(".bienvenido").fadeOut('500',function(){
				$(".registro").fadeIn('500');
			});
		},4000)
	});

	$('.datepicker').datepicker({
		language: 'es',
		defaultViewDate: {year: 1997},
		startView: 'years',
		// format: "yyyy/mm/dd"
	});

	jQuery.validator.addMethod("uploadFile", function (val, element) {
		var size = element.files[0].size;
		if (size > 4194304)
		{return false;} else {return true;}
	}, "El archivo debe ser menor a 4MB");

	$.validator.addMethod("regex", function(value, element, regexp)  {
        if (regexp.constructor != RegExp) regexp = new RegExp(regexp);
        else if (regexp.global) regexp.lastIndex = 0;
        return this.optional(element) || regexp.test(value);
    }, "Por favor, escribe una dirección de correo válida.");

	$('#registro').validate({
		rules: {
			email: {
		        required: true,
		        email: true,
		        regex: /\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/i
		    },
		    password: {
		        required: true,
		        minlength: 6,
		    },
			confirm_password: {
				required: true,
            	minlength: 6,
				equalTo: "#password",
			}
		}
	});

	$('#update-talento').validate({
		rules: {
			cv_file: { 
				required: true, 
				accept: "application/pdf",
				uploadFile:true,
			}
		},
		messages: {
	        cv_file: {
	            accept: "El archivo debe ser en formato PDF.",
	        }
    	}
	});
});
