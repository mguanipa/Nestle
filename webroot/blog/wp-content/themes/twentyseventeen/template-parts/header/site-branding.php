<?php
/**
 * Displays header site branding
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<div class="site-branding">
	<div class="wrap">

		<a href="<?php echo 'https://'.$_SERVER['SERVER_NAME'].'/iniciativaporlosjovenes'; ?>" class="custom-logo-link" rel="home" itemprop="url"><img width="1028" height="250" src="<?php echo 'https://'.$_SERVER['SERVER_NAME'].'/iniciativaporlosjovenes'; ?>/blog/wp-content/uploads/2017/09/cropped-logo.png" class="custom-logo" alt="Noticias" itemprop="logo" srcset="<?php echo 'https://'.$_SERVER['SERVER_NAME'].'/iniciativaporlosjovenes'; ?>/blog/wp-content/uploads/2017/09/cropped-logo.png 1028w, <?php echo 'https://'.$_SERVER['SERVER_NAME'].'/iniciativaporlosjovenes'; ?>/blog/wp-content/uploads/2017/09/cropped-logo-300x73.png 300w, <?php echo 'https://'.$_SERVER['SERVER_NAME'].'/iniciativaporlosjovenes'; ?>/blog/wp-content/uploads/2017/09/cropped-logo-768x187.png 768w, <?php echo 'https://'.$_SERVER['SERVER_NAME'].'/iniciativaporlosjovenes'; ?>/blog/wp-content/uploads/2017/09/cropped-logo-1024x249.png 1024w" sizes="(max-width: 767px) 89vw, (max-width: 1000px) 54vw, (max-width: 1071px) 543px, 580px"></a>

		<div class="site-branding-text">
			<?php
				$url = 'https://'.$_SERVER['SERVER_NAME'].'/iniciativaporlosjovenes';
			?>
			<a href="<?php echo esc_url( 'https://'.$_SERVER['SERVER_NAME'].'/iniciativaporlosjovenes'); ?>" rel="home"> Volver</a>
			<?php if ( is_front_page() ) : ?>
				<h1 class="site-title"><a href="<?php echo esc_url( 'https://'.$_SERVER['SERVER_NAME'].'/iniciativaporlosjovenes'); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
			<?php else : ?>
				<p class="site-title"><a href="<?php echo esc_url( 'https://'.$_SERVER['SERVER_NAME'].'/iniciativaporlosjovenes'); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
			<?php endif; ?>

			<?php
			$description = get_bloginfo( 'description', 'display' );

			if ( $description || is_customize_preview() ) :
			?>
				<p class="site-description"><?php echo $description; ?></p>
			<?php endif; ?>
		</div><!-- .site-branding-text -->

		<?php if ( ( twentyseventeen_is_frontpage() || ( is_home() && is_front_page() ) ) && ! has_nav_menu( 'top' ) ) : ?>
		<a href="#content" class="menu-scroll-down"><?php echo twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ); ?><span class="screen-reader-text"><?php _e( 'Scroll down to content', 'twentyseventeen' ); ?></span></a>
	<?php endif; ?>

	</div><!-- .wrap -->
</div><!-- .site-branding -->
