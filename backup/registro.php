<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="tcal.css" />
<script>
		
	function validarFormulario(){
 
		var txtNombre = document.getElementById('nombre').value;
		var txtApellido = document.getElementById('apellido').value;
		var txtCarrera = document.getElementById('carrera').value;
		var txtFecha = document.getElementById('date').value;
		var txtCorreo = document.getElementById('correo').value;
		var txtPassword = document.getElementById('password').value;
		var txtPassword2 = document.getElementById('password_confirm').value;
		var txtEstado = document.getElementById('estado').value;
		var txtUniversidad = document.getElementById('universidad').value;
 
		var banderaRBTN = false;
 
		//Test campo obligatorio
		if(txtNombre == null || txtNombre.length == 0 || /^\s+$/.test(txtNombre)){
			alert('ERROR: El campo nombre no debe ir vacío o lleno de solamente espacios en blanco');
			return false;
		}
 
		if(txtApellido == null || txtApellido.length == 0 || /^\s+$/.test(txtApellido)){
			alert('ERROR: El campo apellido no debe ir vacío o lleno de solamente espacios en blanco');
			return false;
		} 
		
		if(txtCarrera == null || txtCarrera.length == 0 || /^\s+$/.test(txtCarrera)){
			alert('ERROR: El campo Carrera / Profesión no debe ir vacío o lleno de solamente espacios en blanco');
			return false;
		} 
		
		if(!isNaN(txtFecha)){
			alert('ERROR: Debes indicar tu fecha de nacimiento');
			return false;
		}
				if(!(/\S+@\S+\.\S+/.test(txtCorreo))){
			alert('ERROR: Debe escribir un correo válido');
			return false;
		}
		
		if(txtPassword == null || txtPassword.length == 0 ||/^\s+$/.test(txtPassword)){
			alert('ERROR: El campo Contraseña de puede estar vacío o lleno de solamente espacios en blanco');
			return false;
		} 
		
		if(txtPassword2 == null || txtPassword2.length == 0 ||/^\s+$/.test(txtPassword2)){
			alert('ERROR: Debe Confirmar la contraseña');
			return false;
		} 
		
		if(txtPassword != txtPassword2){
			alert('ERROR: La contraseña y su confirmación no coinciden');
			return false;
		} 
		
		if(txtEstado == 0){
			alert('ERROR: Debe seleccionar un Estado');
			return false;
		}
		
		if(txtUniversidad == 0){
			alert('ERROR: Debe seleccionar una Universidad');
			return false;
		}
		
		 
		return true;
	}
 
	</script>
	<script type="text/javascript" src="tcal.js"></script> 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Nestlé - Iniciativa por los Jóvenes</title>
<style>
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	background-color: #01BEE8;
}
.log_tit {
	font-family: "Century Gothic";
	text-align: center;
	font-weight: bold;
	font-size: 18px;
}
.logregister {
	font-family: "Century Gothic";
	font-size: 14px;
	text-align: left;
}


/* Full-width input fields */
input[type=text], input[type=password] {
    width: 95%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    box-sizing: border-box;
}

/* Set a style for all buttons */
button {
    background-color: #01BEE8;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    width: 95%;
}

button:hover {
    opacity: 0.8;
}

/* Extra styles for the cancel button */
.cancelbtn {
    width: auto;
    padding: 10px 18px;
    background-color: #f44336;
}

/* Center the image and position the close button */
.imgcontainer {
    text-align: center;
    margin: 24px 0 12px 0;
    position: relative;
}

img.avatar {
    width: 40%;
    border-radius: 50%;
}

.container {
    padding: 16px;
}

span.psw {
    float: right;
    padding-top: 16px;
}

/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    padding-top: 0px;
}

/* Modal Content/Box */
.modal-content {
    background-color: #fefefe;
    margin: 5% auto 15% auto; /* 5% from the top, 15% from the bottom and centered */
    border: 1px solid #888;
    width: 25%; /* Could be more or less, depending on screen size */
}

/* The Close Button (x) */
.close {
    position: absolute;
    right: 25px;
    top: 0;
    color: #000;
    font-size: 35px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: red;
    cursor: pointer;
}

/* Add Zoom Animation */
.animate {
    -webkit-animation: animatezoom 0.6s;
    animation: animatezoom 0.6s
}

@-webkit-keyframes animatezoom {
    from {-webkit-transform: scale(0)} 
    to {-webkit-transform: scale(1)}
}
    
@keyframes animatezoom {
    from {transform: scale(0)} 
    to {transform: scale(1)}
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
    span.psw {
       display: block;
       float: none;
    }
    .cancelbtn {
       width: 100%;
    }

body p {
	font-family: "Century Gothic";
}
#form1 p {
	font-family: "Century Gothic";
}
.aaaa {
	font-family: "Century Gothic";
}
.aaaa {	font-family: "Century Gothic";
}
</style>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" bgcolor="#01BEE8">&nbsp;<img src="logoblancoyouth.png" width="328" height="89"><br>
      <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align="center" bgcolor="#FFFFFF"><form action="registrar.php" method="post" name="form1" id="form1" onsubmit="return validarFormulario()" form>
          <p> <span style="font-family: 'Century Gothic'">Nombre
            <input name="nombre" type="text" class="logregister" id="nombre" />
          </span></p>
          <p><span style="font-family: 'Century Gothic'">Apellido </span>
            <input name="apellido" type="text" class="logregister" id="apellido" />
          </p>
          <p><span class="aaaa" style="font-family: 'Century Gothic'">Carrera / Profesión<br />
            </span>
            <input name="carrera" type="text" class="logregister" id="carrera" />
          </p>
          <p style="font-family: 'Century Gothic'">Fecha de nacimiento <br />
          <div>
            <input name="date" type="text" class="tcal" value="" readonly id="date"/>
          </div>
          </p>
          <label for="correo">
          <p><span style="font-family: 'Century Gothic'">Correo </span>
            <input name="correo" type="text" class="logregister" id="correo" />
          </p>
          <p><span style="font-family: 'Century Gothic'">Contraseña</span>
            <input name="password" type="password" class="logregister" id="password" />
          </p>
          <p><span style="font-family: 'Century Gothic'">Confirmar Contraseña </span>
            <input name="password_confirm" type="password" class="logregister" id="password_confirm" />
          </p>
          <p><span style="font-family: 'Century Gothic'">Estado </span><br />
            <select name="estado" class="logregister" id="estado">
              <option value="0" selected="selected">Seleccione</option>
              <option value="Amazonas">Amazonas</option>
              <option value="Anzoátegui">Anzoátegui</option>
              <option value="Apure">Apure</option>
              <option value="Aragua">Aragua</option>
              <option value="Barinas">Barinas</option>
              <option value="Bolívar">Bolívar</option>
              <option value="Carabobo">Carabobo</option>
              <option value="Cojedes">Cojedes</option>
              <option value="Delta Amacuro">Delta Amacuro</option>
              <option value="Distrito Capital">Distrito Capital</option>
              <option value="Falcón">Falcón</option>
              <option value="Guárico">Guárico</option>
              <option value="Lara">Lara</option>
              <option value="Mérida">Mérida</option>
              <option value="Miranda">Miranda</option>
              <option value="Monagas">Monagas</option>
              <option value="Nueva Esparta">Nueva Esparta</option>
              <option value="Portuguesa">Portuguesa</option>
              <option value="Sucre">Sucre</option>
              <option value="Táchira">Táchira</option>
              <option value="Trujillo">Trujillo</option>
              <option value="Vargas">Vargas</option>
              <option value="Yaracuy">Yaracuy</option>
              <option value="Zulia">Zulia</option>
            </select>
          </p>
          <p style="font-family: 'Century Gothic'">Universidad <br />
            <select name="universidad" class="logregister" id="universidad">
              <option value="0" selected="selected">Seleccione </option>
              <option value=" Universidad Simón Bolívar "> Universidad Simón Bolívar </option>
              <option value=" Universidad Centroccidental Lisandro Alvarado "> Universidad Centroccidental Lisandro Alvarado </option>
              <option value=" Universidad Bolivariana de Venezuela "> Universidad Bolivariana de Venezuela </option>
              <option value=" Universidad Nacional Abierta "> Universidad Nacional Abierta </option>
              <option value=" Universidad de los Pueblos del Sur "> Universidad de los Pueblos del Sur </option>
              <option value=" Universidad Indígena de Venezuela "> Universidad Indígena de Venezuela </option>
              <option value=" Universidad Politécnica Territorial del Norte de Monagas Ludovico Silva "> Universidad Politécnica Territorial del Norte de Monagas Ludovico Silva </option>
              <option value=" Universidad Politécnica Territorial del estado Portuguesa Juan de Jesús Montilla "> Universidad Politécnica Territorial del estado Portuguesa Juan de Jesús Montilla </option>
              <option value=" Universidad Politécnica Territorial del Oeste de Sucre Clodosbaldo Russián "> Universidad Politécnica Territorial del Oeste de Sucre Clodosbaldo Russián </option>
              <option value=" Universidad Politécnica Territorial de Paria Luis Mariano Rivera "> Universidad Politécnica Territorial de Paria Luis Mariano Rivera </option>
              <option value=" Universidad Politécnica Territorial del estado Mérida Kléber Ramírez "> Universidad Politécnica Territorial del estado Mérida Kléber Ramírez </option>
              <option value=" Instituto Universitario Politécnico de las Fuerzas Armadas Nacionales "> Instituto Universitario Politécnico de las Fuerzas Armadas Nacionales </option>
              <option value=" Instituto Universitario de Tecnología José Antonio Anzoátegui "> Instituto Universitario de Tecnología José Antonio Anzoátegui </option>
              <option value=" Instituto Universitario de Tecnología Bomberil "> Instituto Universitario de Tecnología Bomberil </option>
              <option value=" Instituto Nacional de Capacitación y Educación Socialista "> Instituto Nacional de Capacitación y Educación Socialista </option>
              <option value=" Universidad Central de Venezuela "> Universidad Central de Venezuela </option>
              <option value=" Universidad de Los Andes "> Universidad de Los Andes </option>
              <option value=" Universidad de Carabobo "> Universidad de Carabobo </option>
              <option value=" Universidad de Oriente "> Universidad de Oriente </option>
              <option value=" Universidad del Zulia "> Universidad del Zulia </option>
              <option value=" Universidad Nacional Experimental Politécnica de la Fuerza Armada Nacional "> Universidad Nacional Experimental Politécnica de la Fuerza Armada Nacional </option>
              <option value=" Universidad Nacional Experimental de la Seguridad "> Universidad Nacional Experimental de la Seguridad </option>
              <option value=" Universidad Pedagógica Experimental Libertador "> Universidad Pedagógica Experimental Libertador </option>
              <option value=" Universidad Nacional Experimental de las Artes "> Universidad Nacional Experimental de las Artes </option>
              <option value=" Universidad Central de Diseño "> Universidad Central de Diseño </option>
              <option value=" Universidad Nacional Experimental de Guayana "> Universidad Nacional Experimental de Guayana </option>
              <option value=" Universidad Nacional Experimental de Los Llanos Centrales Rómulo Gallegos "> Universidad Nacional Experimental de Los Llanos Centrales Rómulo Gallegos </option>
              <option value=" Universidad Nacional Experimental de los Llanos Occidentales Ezequiel Zamora "> Universidad Nacional Experimental de los Llanos Occidentales Ezequiel Zamora </option>
              <option value=" Universidad Experimental Venezolana de los Hidrocarburos "> Universidad Experimental Venezolana de los Hidrocarburos </option>
              <option value=" Universidad Nacional Experimental del Táchira "> Universidad Nacional Experimental del Táchira </option>
              <option value=" Universidad Nacional Experimental Francisco de Miranda "> Universidad Nacional Experimental Francisco de Miranda </option>
              <option value=" Universidad Nacional Experimental Politécnica Antonio José de Sucre "> Universidad Nacional Experimental Politécnica Antonio José de Sucre </option>
              <option value=" Universidad Nacional Experimental Rafael María Baralt "> Universidad Nacional Experimental Rafael María Baralt </option>
              <option value=" Universidad Nacional Experimental Simón Rodríguez "> Universidad Nacional Experimental Simón Rodríguez </option>
              <option value=" Universidad Nacional Experimental Marítima del Caribe "> Universidad Nacional Experimental Marítima del Caribe </option>
              <option value=" Universidad Nacional Experimental de Yaracuy "> Universidad Nacional Experimental de Yaracuy </option>
              <option value=" Universidad Nacional Experimental Sur del Lago Jesús María Semprum "> Universidad Nacional Experimental Sur del Lago Jesús María Semprum </option>
              <option value=" Universidad Gran Mariscal de Ayacucho "> Universidad Gran Mariscal de Ayacucho </option>
              <option value=" Universidad Alejandro de Humboldt "> Universidad Alejandro de Humboldt </option>
              <option value=" Universidad Alonso de Ojeda "> Universidad Alonso de Ojeda </option>
              <option value=" Universidad Arturo Michelena "> Universidad Arturo Michelena </option>
              <option value=" Universidad Bicentenaria de Aragua "> Universidad Bicentenaria de Aragua </option>
              <option value=" Universidad Rafael Urdaneta "> Universidad Rafael Urdaneta </option>
              <option value=" Universidad Católica Andrés Bello "> Universidad Católica Andrés Bello </option>
              <option value=" Universidad Católica Cecilio Acosta "> Universidad Católica Cecilio Acosta </option>
              <option value=" Universidad Católica del Táchira "> Universidad Católica del Táchira </option>
              <option value=" Universidad Católica Santa Rosa "> Universidad Católica Santa Rosa </option>
              <option value=" Universidad de Falcón "> Universidad de Falcón </option>
              <option value=" Universidad de Margarita "> Universidad de Margarita </option>
              <option value=" Universidad del Norte "> Universidad del Norte </option>
              <option value=" Universidad Dr. José Gregorio Hernández "> Universidad Dr. José Gregorio Hernández </option>
              <option value=" Universidad Dr. Rafael Belloso Chacín "> Universidad Dr. Rafael Belloso Chacín </option>
              <option value=" Universidad Fermín Toro "> Universidad Fermín Toro </option>
              <option value=" Universidad José Antonio Páez "> Universidad José Antonio Páez </option>
              <option value=" Universidad José María Vargas "> Universidad José María Vargas </option>
              <option value=" Universidad Politécnica de Valencia "> Universidad Politécnica de Valencia </option>
              <option value=" Universidad Metropolitana "> Universidad Metropolitana </option>
              <option value=" Universidad Monteávila "> Universidad Monteávila </option>
              <option value=" Universidad Nueva Esparta "> Universidad Nueva Esparta </option>
              <option value=" Universidad Panamericana del Puerto "> Universidad Panamericana del Puerto </option>
              <option value=" Universidad Santa Inés "> Universidad Santa Inés </option>
              <option value=" Universidad Santa María "> Universidad Santa María </option>
              <option value=" Universidad Tecnológica del Centro "> Universidad Tecnológica del Centro </option>
              <option value=" Universidad Valle del Momboy "> Universidad Valle del Momboy </option>
              <option value=" Universidad Yacambú "> Universidad Yacambú </option>
              <option value=" Instituto Universitario Eclesiástico Santo Tomás de Aquino "> Instituto Universitario Eclesiástico Santo Tomás de Aquino </option>
              <option value=" Instituto Universitario de Tecnología Industrial "> Instituto Universitario de Tecnología Industrial </option>
              <option value=" Instituto Universitario Politécnico ">Instituto Universitario Politécnico &quot;Santiago Mariño&quot; </option>
              <option value=" Instituto Universitario Tecnológico de Valencia "> Instituto Universitario Tecnológico de Valencia </option>
              <option value=" Instituto Universitario de Tecnología Juan Pablo Pérez Alfonso "> Instituto Universitario de Tecnología Juan Pablo Pérez Alfonso </option>
              <option value=" Instituto Universitario de Tecnología de Administración "> Instituto Universitario de Tecnología de Administración </option>
              <option value=" Instituto Universitario Tecnológico Américo Vespucio "> Instituto Universitario Tecnológico Américo Vespucio </option>
              <option value=" Instituto Universitario Tecnológico Rodolfo Loero Arismendi "> Instituto Universitario Tecnológico Rodolfo Loero Arismendi </option>
              <option value=" Instituto Superior Universitario de Mercadotecnia "> Instituto Superior Universitario de Mercadotecnia </option>
              <option value=" Instituto Universitario de Nuevas Profesiones "> Instituto Universitario de Nuevas Profesiones </option>
              <option value=" Instituto Universitario Tecnológico de Seguridad Industrial "> Instituto Universitario Tecnológico de Seguridad Industrial </option>
              <option value=" Instituto Universitario de Tecnología Antonio José de Sucre "> Instituto Universitario de Tecnología Antonio José de Sucre </option>
              <option value=" Instituto Universitario Carlos Soublette "> Instituto Universitario Carlos Soublette </option>
              <option value=" Colegio Universitario de Administración y Mercadeo "> Colegio Universitario de Administración y Mercadeo </option>
              <option value=" Colegio Universitario Fermín Toro "> Colegio Universitario Fermín Toro </option>
              <option value=" Colegio Universitario Monseñor de Talavera "> Colegio Universitario Monseñor de Talavera </option>
              <option value=" Instituto de Especialidades Aeronáuticas "> Instituto de Especialidades Aeronáuticas </option>
              <option value=" Instituto Universitario de Tecnología Rufino Blanco Fombona "> Instituto Universitario de Tecnología Rufino Blanco Fombona </option>
            </select>
          </p>
          <p> <span style="font-family: 'Century Gothic'">
            <button type="submit"><br />
              Registro<br />
              <br />
              </button>
          </span></p>
        </form></td>
      </tr>
    </table></td>
  </tr>
</table>
<br>
<p>&nbsp;</p>
</body>
</html>