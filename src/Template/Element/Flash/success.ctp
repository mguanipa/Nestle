<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="alert alert-success mt-5" role="alert" onclick="this.classList.add('hidden');">
                 <?= $message ?>
                 </div>
