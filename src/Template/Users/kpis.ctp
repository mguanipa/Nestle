  <?= $this->Html->css('/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css') ?>        


  <div class="right_col" role="main">
        <!-- top tiles -->
          <div class="row tile_count">
            <div class="col-md-2 col-sm-12 col-xs-12 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total usuarios</span>
              <div class="count"><?php echo $talentos?></div>
            </div> 
            <div class="col-md-2 col-sm-12 col-xs-12 tile_stats_count">
              <span class="count_top"><i class="fa fa-facebook"></i> Compartidos facebook</span>
              <div class="count"><?php echo $facebook?></div>
            </div>            
            
            
            <div class="col-md-3 col-sm-12 col-xs-12 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Talentos que han terminado los e-learning.</span>
              <div class="count"><?php echo $Finish?></div>             
            </div>

           

            <div class="col-md-3 col-sm-12 col-xs-12 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Talentos con CV menores de 30 años</span>
              <div class="count green"><?php echo $edadcount?></div>
              
            </div>
             <div class="col-md-2 col-sm-12 col-xs-12 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Cv descargados. </span>
              <div class="count"><?php echo $download ?></div>             
            </div>
            
          </div>
          <!-- /top tiles -->
       

           <div class="row">
             <div class="col-md-6 col-sm-12 col-xs-12">
              <div class="x_panel ">
                <div class="x_title">
                  <h2>Reporte de abandono de los e-learning </h2>
                
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <h4>Pasos donde han llegado los usuarios:</h4>
                  <?php for ($i=1; $i <7 ; $i++) { ?>
                    
               
                  <div class="widget_summary">
                    <div class="w_left w_25">
                      <span>Paso <?php echo $i;?></span>
                    </div>
                    <div class="w_center w_55">
                      <div class="progress">
                        <div class="progress-bar bg-green" role="progressbar" aria-valuenow="<?php ($pasos[$i]*100)/ $talentos ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo ($pasos[$i]*100)/ $talentos ?>%;">
                          <span class="sr-only"><?php echo ($pasos[$i]*100)/ $talentos ?>% Complete</span>
                        </div>
                      </div>
                    </div>
                    <div class="w_right w_20">
                      <?php if($pasos[$i]>0){ ?>
                      <button onclick="buscar(<?php echo $i; ?>);"> ver</button>
                      <?php }?>
                      <span><?php echo $pasos[$i]." de ".$talentos ?></span>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                    <?php } ?>


                </div>
              </div>
            </div>

            <div id='busqueda' style="display: none;" class="row">
             <div class="col-md-6 col-sm-12 col-xs-12">
              <div class="x_panel ">
                 <h4>Lista de usuarios:</h4>
                  <div id="resultado">
                      
                  
                
                  </div>
              </div>
            </div>

          </div>
          </div>


          



        

        <script>
        function buscar(id){
              $("#busqueda").show();
            $.ajax({
                  data:  {step:id},
                  url:   '<?php echo $this->Url->build("/users/progresos"); ?>',
                  type:  'post',
                  beforeSend: function () {
                          $("#resultado").html("Procesando, espere por favor...");
                  },
                  success:  function (response) {
                          $("#resultado").html(response);
                  }
                  });
            }
        </script>



</div>
   <?= $this->Html->script('/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') ?>   