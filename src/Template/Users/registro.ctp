<!-- <div class="container">
    <div class="row agradecimiento">
        <div class="col-12 align-self-center text-center">
            <img class="img-fluid" src="img/registro/logo-small.png">
            <h1>Gracias<br> por Registrarte
            </h1>
            <h3>En este portal de Iniciativa por los jóvenes <strong>encontrarás e-learning o capacitaciones que te brindarán la oportunidad de subir tu CV</strong> y que Nestlé y sus aliados puedan contactarte</h3>
        </div>
    </div>
</div> -->
<div class="container">

    <!-- Navbar -->
   <nav class="navbar navbar-expand-lg navbar-light">
        <a class="navbar-brand mx-center" href="<?php echo $this->Url->build('/'); ?>">
            <?php echo $this->Html->image("/frontend/img/logo.png"); ?>
            
        </a>
         <a style="color: #00a5da;" href="<?php echo $this->Url->build('/'); ?>"><i class="fa fa-sign-out" aria-hidden="true"></i></a>
    </nav>

    <form action="<?php echo $this->Url->build('/users/registro'); ?>" enctype='multipart/form-data' method="post" id="registro">
        <div class="row">
            <div class="col-12 col-md-6 p-5 align-self-stretch d-block first-col-login">
                <div class="form-group">
                    <label for="">Nombres:</label> <input name="first_name" class="form-control" id="" type="text" required>
                </div>
                <div class="form-group">
                    <label for="">Apellidos:</label> <input name="last_name" class="form-control" id="" type="text" required>
                </div>
                <div class="form-group">
                    <label for="">Carrera/Profesión:</label> <select name="carrera" class="form-control" required>
                        <option></option>
                        <option value="Abogacía">Abogacía</option>
                        <option value="Actuaría">Actuaría</option>
                        <option value="Acuicultura">Agricultura</option>
                        <option value="Adm. De Empresas">Adm. De Empresas</option>
                        <option value="Adm y Gestión Pública">Adm y Gestión Pública</option>
                        <option value="Adm Agropecuaria">Adm Agropecuaria</option>
                        <option value="Agrimensor">Agrimensor</option>
                        <option value="Agronegocios">Agronegocios</option>
                        <option value="Antropología">Antropología</option>
                        <option value="Análisis de Sistemas">Análisis de Sistemas</option>
                        <option value="Apoderado Aduanal">Apoderado Aduanal</option>
                        <option value="Arqueología">Arqueología</option>
                        <option value="Asesoría Legal Internacional">Asesoría Legal Internacional</option>
                        <option value="Asesoría Comercio Exterior">Asesoría Comercio Exterior</option>
                        <option value="Astronomía">Astronomía</option>
                        <option value="Bellas Artes">Bellas Artes</option>
                        <option value="BioFísica">BioFísica</option>
                        <option value="BioIngeniería">BioIngeniería</option>
                        <option value="Bliología">Bliología</option>
                        <option value="Bioquímica">Bioquímica</option>
                        <option value="Biotecnología">Biotecnología</option>
                        <option value="Call Centes">Call Centes</option>
                        <option value="Cartografía">Cartografía</option>
                        <option value="Ciencias Físicas">Ciencias Físicas</option>
                        <option value="Ciencias Políticas">Ciencias Políticas</option>
                        <option value="Ciencias de la Educación">Ciencias de la Educación</option>
                        <option value="Comercio Int/Ext">Comercio Int/Ext</option>
                        <option value="Computación / Informática">Computación / Informática</option>
                        <option value="Comunciación Audiovisual">Comunciación Audiovisual</option>
                        <option value="Comunicación Social">Comunicación Social</option>
                        <option value="Construcción /Obras civiles">Construcción /Obras civiles</option>
                        <option value="Contabilidad / Auditoría">Contabilidad / Auditoría</option>
                        <option value="Dibujo Técnico">Dibujo Técnico</option>
                        <option value="Diseño Gráfico">Diseño Gráfico</option>
                        <option value="Diseño Industrial">Diseño Industrial</option>
                        <option value="Diseño Web">Diseño Web</option>
                        <option value="Ecología">Ecología</option>
                        <option value="Economía">Economía</option>
                        <option value="Educación">Educación</option>
                        <option value="Electricidad">Electricidad</option>
                        <option value="Electrónica">Electrónica</option>
                        <option value="Enfermería">Enfermería</option>
                        <option value="Enología">Enología</option>
                        <option value="Estadística">Estadística</option>
                        <option value="Farmacia">Farmacia</option>
                        <option value="Filosofía">Filosofía</option>
                        <option value="Finanzas">Finanzas</option>
                        <option value="Fisioterapia">Fisioterapia</option>
                        <option value="Fotografía">Fotografía</option>
                        <option value="Gastronomía">Gastronomía</option>
                        <option value="Geofísica">Geofísica</option>
                        <option value="Geología">Geología</option>
                        <option value="Hidráulica">Hidráulica</option>
                        <option value="Historia">Historia</option>
                        <option value="Hotelería">Hotelería</option>
                        <option value="Ing. Aerospacial">Ing. Aerospacial</option>
                        <option value="Ing. Agropecuario">Ing. Agropecuario</option>
                        <option value="Ing. Agrónomo">Ing. Agrónomo</option>
                        <option value="Ing. Alimentos">Ing. Alimentos</option>
                        <option value="Ing. Ambiental">Ing. Ambiental</option>
                        <option value="Ing. Comercial">Ing. Comercial</option>
                        <option value="Ing. Electrónica">Ing. Electrónica</option>
                        <option value="Ing. Eléctrica">Ing. Eléctrica</option>
                        <option value="Ing. Forestal">Ing. Forestal</option>
                        <option value="Ing. Hidáulica">Ing. Hidáulica</option>
                        <option value="Ing. Industrial">Ing. Industrial</option>
                        <option value="Ing. Informática">Ing. Informática</option>
                        <option value="Ing. Matemática">Ing. Matemática</option>
                        <option value="Ing. Mecánica">Ing. Mecánica</option>
                        <option value="Ing. Naval">Ing. Naval</option>
                        <option value="Ing. Obras Civiles / Construcciones">Ing. Obras Civiles / Construcciones</option>
                        <option value="Ing. Química">Ing. Química</option>
                        <option value="Ing. Recursos Hídricos">Ing. Recursos Hídricos</option>
                        <option value="Ing. Sonido">Ing. Sonido</option>
                        <option value="Ing. Telecomunicaciones">Ing. Telecomunicaciones</option>
                        <option value="Ing. Transporte">Ing. Transporte</option>
                        <option value="Ing. Materiales">Ing. Materiales</option>
                        <option value="Ing. Sistemas">Ing. Sistemas</option>
                        <option value="Ing. Vial">Ing. Vial</option>
                        <option value="Intérprete">Intérprete</option>
                        <option value="Kinesiología">Kinesiología</option>
                        <option value="Laboratorio">Laboratorio</option>
                        <option value="Literatura">Literatura</option>
                        <option value="Marketing">Marketing</option>
                        <option value="Matemáticas">Matemáticas</option>
                        <option value="Mecánica">Mecánica</option>
                        <option value="Medicina">Medicina</option>
                        <option value="Medio Ambiente">Medio Ambiente</option>
                        <option value="Mercadotecnia ">Mercadotecnia</option>
                        <option value="Nutrición">Nutrición</option>
                        <option value="Odontología">Odontología</option>
                        <option value="Periodismo">Periodismo</option>
                        <option value="Programación">Programación</option>
                        <option value="Psicología">Psicología</option>
                        <option value="Psicopedagogía">Psicopedagogía</option>
                        <option value="Publicidad">Publicidad</option>
                        <option value="Química">Química</option>
                        <option value="Recursos Humanos /Relaciones Industriales">Recursos Humanos /Relaciones Industriales</option>
                        <option value="Relaciones Internacionales">Relaciones Internacionales</option>
                        <option value="Relaciones Publicas">Relaciones Publicas</option>
                        <option value="Secretariado">Secretariado</option>
                        <option value="Seguridad Industrial">Seguridad Industrial</option>
                        <option value="Seguros">Seguros</option>
                        <option value="Sociología">Sociología</option>
                        <option value="Técnico">Técnico</option>
                        <option value="Turismo">Turismo</option>
                        <option value="Ventas">Ventas</option>
                        <option value="Veterinaria">Veterinaria</option>
                        <option value="Otro">Otro</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Fecha de nacimiento:</label> <input name="fecha_nacimiento" class="form-control datepicker" id="birthday-date" type="text" required>
                </div>
                <div class="form-group">
                    <label for="">Estado:</label> <select name="estado" class="form-control" id="" required>
                        <option></option>
                        <option value="Amazonas">Amazonas</option>
                        <option value="Anzoátegui">Anzoátegui</option>
                        <option value="Apure">Apure</option>
                        <option value="Aragua">Aragua</option>
                        <option value="Barinas">Barinas</option>
                        <option value="Bolívar">Bolívar</option>
                        <option value="Carabobo">Carabobo</option>
                        <option value="Cojedes">Cojedes</option>
                        <option value="Delta Amacuro">Delta Amacuro</option>
                        <option value="Dependencias Federales">Dependencias Federales</option>
                        <option value="Distrito Capital">Distrito Capital</option>
                        <option value="Falcón">Falcón</option>
                        <option value="Guárico">Guárico</option>
                        <option value="Lara">Lara</option>
                        <option value="Mérida">Mérida</option>
                        <option value="Miranda">Miranda</option>
                        <option value="Monagas">Monagas</option>
                        <option value="Nueva Esparta">Nueva Esparta</option>
                        <option value="Portuguesa">Portuguesa</option>
                        <option value="Sucre">Sucre</option>
                        <option value="Táchira">Táchira</option>
                        <option value="Trujillo">Trujillo</option>
                        <option value="Vargas">Vargas</option>
                        <option value="Yaracuy">Yaracuy</option>
                        <option value="Zulia">Zulia</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Universidad:</label> <select name="universidad" class="form-control" id="" required>  
                        <option></option>
                        <option value="Colegio Universitario de Administración y Mercadeo">Colegio Universitario de Administración y Mercadeo</option>
                        <option value="Colegio Universitario Fermín Toro">Colegio Universitario Fermín Toro</option>
                        <option value="Colegio Universitario Monseñor de Talavera">Colegio Universitario Monseñor de Talavera</option>
                        <option value="Instituto de Especialidades Aeronáuticas">Instituto de Especialidades Aeronáuticas</option>
                        <option value="Instituto Nacional de Capacitación y Educación Socialista">Instituto Nacional de Capacitación y Educación Socialista</option>
                        <option value="Instituto Superior Universitario de Mercadotecnia">Instituto Superior Universitario de Mercadotecnia</option>
                        <option value="Instituto Universitario Carlos Soublette">Instituto Universitario Carlos Soublette</option>
                        <option value="Instituto Universitario de Nuevas Profesiones">Instituto Universitario de Nuevas Profesiones</option>
                        <option value="Instituto Universitario de Tecnología Antonio José de Sucre">Instituto Universitario de Tecnología Antonio José de Sucre</option>
                        <option value="Instituto Universitario de Tecnología Antonio Ricaurte">Instituto Universitario de Tecnología Antonio Ricaurte</option>
                        <option value="Instituto Universitario de Tecnología Bomberil">Instituto Universitario de Tecnología Bomberil</option>
                        <option value="Instituto Universitario de Tecnología de Administración">Instituto Universitario de Tecnología de Administración</option>
                        <option value="Instituto Universitario de Tecnología Industrial">Instituto Universitario de Tecnología Industrial</option>
                        <option value="Instituto Universitario de Tecnología José Antonio Anzoátegui">Instituto Universitario de Tecnología José Antonio Anzoátegui</option>
                        <option value="Instituto Universitario de Tecnología Juan Pablo Pérez Alfonso">Instituto Universitario de Tecnología Juan Pablo Pérez Alfonso</option>
                        <option value="Instituto Universitario de Tecnología Rufino Blanco Fombona">Instituto Universitario de Tecnología Rufino Blanco Fombona</option>
                        <option value="Instituto Universitario Eclesiástico Santo Tomás de Aquino">Instituto Universitario Eclesiástico Santo Tomás de Aquino</option>
                        <option value="Instituto Universitario Politécnico "Santiago Mariño"">Instituto Universitario Politécnico "Santiago Mariño"</option>
                        <option value="Instituto Universitario Politécnico de las Fuerzas Armadas Nacionales">Instituto Universitario Politécnico de las Fuerzas Armadas Nacionales</option>
                        <option value="Instituto Universitario Tecnológico Américo Vespucio">Instituto Universitario Tecnológico Américo Vespucio</option>
                        <option value="Instituto Universitario Tecnológico de Seguridad Industrial">Instituto Universitario Tecnológico de Seguridad Industrial</option>
                        <option value="Instituto Universitario Tecnológico de Valencia">Instituto Universitario Tecnológico de Valencia</option>
                        <option value="Instituto Universitario Tecnológico Rodolfo Loero Arismendi">Instituto Universitario Tecnológico Rodolfo Loero Arismendi</option>
                        <option value="Universidad Alejandro de Humboldt">Universidad Alejandro de Humboldt</option>
                        <option value="Universidad Alonso de Ojeda">Universidad Alonso de Ojeda</option>
                        <option value="Universidad Arturo Michelena">Universidad Arturo Michelena</option>
                        <option value="Universidad Bicentenaria de Aragua">Universidad Bicentenaria de Aragua</option>
                        <option value="Universidad Bolivariana de Venezuela">Universidad Bolivariana de Venezuela</option>
                        <option value="Universidad Católica Andrés Bello">Universidad Católica Andrés Bello</option>
                        <option value="Universidad Católica Cecilio Acosta">Universidad Católica Cecilio Acosta</option>
                        <option value="Universidad Católica del Táchira">Universidad Católica del Táchira</option>
                        <option value="Universidad Católica Santa Rosa">Universidad Católica Santa Rosa</option>
                        <option value="Universidad Central de Diseño">Universidad Central de Diseño</option>
                        <option value="Universidad Central de Venezuela">Universidad Central de Venezuela</option>
                        <option value="Universidad Centroccidental Lisandro Alvarado">Universidad Centroccidental Lisandro Alvarado</option>
                        <option value="Universidad de Carabobo">Universidad de Carabobo</option>
                        <option value="Universidad de Falcón">Universidad de Falcón</option>
                        <option value="Universidad de Los Andes">Universidad de Los Andes</option>
                        <option value="Universidad de los Pueblos del Sur">Universidad de los Pueblos del Sur</option>
                        <option value="Universidad de Margarita">Universidad de Margarita</option>
                        <option value="Universidad de Oriente">Universidad de Oriente</option>
                        <option value="Universidad del Norte">Universidad del Norte</option>
                        <option value="Universidad del Zulia">Universidad del Zulia</option>
                        <option value="Universidad Dr. José Gregorio Hernández">Universidad Dr. José Gregorio Hernández</option>
                        <option value="Universidad Dr. Rafael Belloso Chacín">Universidad Dr. Rafael Belloso Chacín</option>
                        <option value="Universidad Experimental Venezolana de los Hidrocarburos">Universidad Experimental Venezolana de los Hidrocarburos</option>
                        <option value="Universidad Fermín Toro">Universidad Fermín Toro</option>
                        <option value="Universidad Gran Mariscal de Ayacucho">Universidad Gran Mariscal de Ayacucho</option>
                        <option value="Universidad Indígena de Venezuela">Universidad Indígena de Venezuela</option>
                        <option value="Universidad José Antonio Páez">Universidad José Antonio Páez</option>
                        <option value="Universidad José María Vargas">Universidad José María Vargas</option>
                        <option value="Universidad Metropolitana">Universidad Metropolitana</option>
                        <option value="Universidad Monteávila">Universidad Monteávila</option>
                        <option value="Universidad Nacional Abierta">Universidad Nacional Abierta</option>
                        <option value="Universidad Nacional Experimental de Guayana">Universidad Nacional Experimental de Guayana</option>
                        <option value="Universidad Nacional Experimental de la Seguridad">Universidad Nacional Experimental de la Seguridad</option>
                        <option value="Universidad Nacional Experimental de las Artes">Universidad Nacional Experimental de las Artes</option>
                        <option value="Universidad Nacional Experimental de Los Llanos Centrales Rómulo Gallegos">Universidad Nacional Experimental de Los Llanos Centrales Rómulo Gallegos</option>
                        <option value="Universidad Nacional Experimental de los Llanos Occidentales Ezequiel Zamora">Universidad Nacional Experimental de los Llanos Occidentales Ezequiel Zamora</option>
                        <option value="Universidad Nacional Experimental de Yaracuy">Universidad Nacional Experimental de Yaracuy</option>
                        <option value="Universidad Nacional Experimental del Táchira">Universidad Nacional Experimental del Táchira</option>
                        <option value="Universidad Nacional Experimental Francisco de Miranda">Universidad Nacional Experimental Francisco de Miranda</option>
                        <option value="Universidad Nacional Experimental Marítima del Caribe">Universidad Nacional Experimental Marítima del Caribe</option>
                        <option value="Universidad Nacional Experimental Politécnica Antonio José de Sucre">Universidad Nacional Experimental Politécnica Antonio José de Sucre</option>
                        <option value="Universidad Nacional Experimental Politécnica de la Fuerza Armada Nacional">Universidad Nacional Experimental Politécnica de la Fuerza Armada Nacional</option>
                        <option value="Universidad Nacional Experimental Rafael María Baralt">Universidad Nacional Experimental Rafael María Baralt</option>
                        <option value="Universidad Nacional Experimental Simón Rodríguez">Universidad Nacional Experimental Simón Rodríguez</option>
                        <option value="Universidad Nacional Experimental Sur del Lago Jesús María Semprum">Universidad Nacional Experimental Sur del Lago Jesús María Semprum</option>
                        <option value="Universidad Nueva Esparta">Universidad Nueva Esparta</option>
                        <option value="Universidad Panamericana del Puerto">Universidad Panamericana del Puerto</option>
                        <option value="Universidad Pedagógica Experimental Libertador">Universidad Pedagógica Experimental Libertador</option>
                        <option value="Universidad Politécnica de Valencia">Universidad Politécnica de Valencia</option>
                        <option value="Universidad Politécnica Territorial de Paria Luis Mariano Rivera">Universidad Politécnica Territorial de Paria Luis Mariano Rivera</option>
                        <option value="Universidad Politécnica Territorial del estado Mérida Kléber Ramírez">Universidad Politécnica Territorial del estado Mérida Kléber Ramírez</option>
                        <option value="Universidad Politécnica Territorial del estado Portuguesa Juan de Jesús Montilla">Universidad Politécnica Territorial del estado Portuguesa Juan de Jesús Montilla</option>
                        <option value="Universidad Politécnica Territorial del Norte de Monagas Ludovico Silva">Universidad Politécnica Territorial del Norte de Monagas Ludovico Silva</option>
                        <option value="Universidad Politécnica Territorial del Oeste de Sucre Clodosbaldo Russián">Universidad Politécnica Territorial del Oeste de Sucre Clodosbaldo Russián</option>
                        <option value="Universidad Rafael Urdaneta">Universidad Rafael Urdaneta</option>
                        <option value="Universidad Santa Inés">Universidad Santa Inés</option>
                        <option value="Universidad Santa María">Universidad Santa María</option>
                        <option value="Universidad Simón Bolívar">Universidad Simón Bolívar</option>
                        <option value="Universidad Tecnológica del Centro">Universidad Tecnológica del Centro</option>
                        <option value="Universidad Valle del Momboy">Universidad Valle del Momboy</option>
                        <option value="Universidad Yacambú">Universidad Yacambú</option>
                        <option value="Otro">Otro</option>
                    </select>
                </div>
            </div>
            <div class="col-12 col-md-6 p-5 align-self-stretch d-block second-col-login">
                <div class="form-group">
                    <label for="">Usuario:</label> <input name="Username" class="form-control" id="" type="username" required>
                </div>
                <div class="form-group">
                    <label for="">Correo:</label> <input name="email" class="form-control" type="email" required>
                </div>
                <div class="form-group">
                    <label for="">Contraseña:</label> <input name="password" class="form-control" id="password" type="password">
                </div>
                <div class="form-group">
                    <label for="">Confirmar Contraseña:</label> <input name="confirm_password" class="form-control" id="confirm_password" type="password">
                </div>

                <button class="btn btn-primary mx-auto mt-5" type="submit">Registro</button>
                 <?= $this->Flash->render() ?>
            </div>
        </div>
    </form>
</div>