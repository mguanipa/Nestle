<div class="container">
    <div class="row bienvenido">
        <div class="col-12 align-self-center text-center">
            <?php echo $this->Html->image("/frontend/img/registro/logo.png", ['class' => 'img-fluid']); ?>
            <h1>¡Regístrate!</h1>
            <h3>Aprenderás habilidades para conseguir <strong>tu primer empleo</strong></h3>
        </div>
    </div>
</div>
<div class="container registro">

    <!-- Navbar -->
  <nav class="navbar navbar-expand-lg navbar-light">
        <a class="navbar-brand mx-center" href="<?php echo $this->Url->build('/'); ?>">
            <?php echo $this->Html->image("/frontend/img/logo.png"); ?>
           
        </a>
         <a style="color: #00a5da;" href="<?php echo $this->Url->build('/'); ?>"><i class="fa fa-sign-out" aria-hidden="true"></i></a>
    </nav>

    <div class="row">
        <div class="col-12 col-md-6 p-5 order-md-2 align-self-stretch align-items-center d-flex second-col-login">
            <form  method="post" accept-charset="utf-8" action="<?php echo $this->Url->build('/users/login'); ?>">
                <div class="form-group">
                    <label for="">Usuario:</label>
                    <input type="text" name="Username" class="form-control" id="">
                </div>
                <div class="form-group">
                    <label for="">Contraseña:</label>
                    <input type="password" name="password" class="form-control" id="">
                </div>
                 <p class="text-center"> <a href="<?php echo $this->Url->build('/users/forgot_password', true); ?>" style="color:white;"> ¿Olvidaste tu cuenta? </a></p>
                <button type="submit" class="btn btn-primary mx-auto mt-5">Iniciar Sesión</button>
                <?= $this->Flash->render() ?>
            </form>
        </div>
        <div class="col-12 col-md-6 p-5 order-md-1 align-self-stretch align-items-center d-flex first-col-login">
            <a class="mx-auto" href="<?php echo $this->Url->build('/users/registro', true); ?> "><button type="button" class="btn btn-primary">Registro</button></a>
        </div>
    </div>
</div>
