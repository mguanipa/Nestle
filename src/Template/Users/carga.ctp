
      <?= $this->Html->css('/vendors/iCheck/skins/flat/green.css') ?>  

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Carga Masiva <small><strong></strong></small></h3>
              </div>
            </div>
          </div>

          <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Cargar el archivo<small></small></h2>
                 
                  <div class="clearfix"></div>
                  </div>

                  <div class="x_content">
                    <form action="" method="POST" enctype="multipart/form-data">
                      <input type="file" name="archivo">
                      <br>
                      <button id="cargar" name="cargar" type="submit" class="btn btn-primary btn-secondary fileinput-upload fileinput-upload-button"><i class="fa fa-upload"></i> <span class="hidden-xs">Cargar archivo</span></button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
        </div>
        <!-- /page content -->


<!-- Validacion de la extension del archivo by Vikua -->

<script type="text/javascript">
$(document).ready(function() {

  $("#cargar").on("click", function() {
    var extension = $("#archivo-submittedfile").val().split('.').pop();
    if (['xls','xlsx', 'csv'].indexOf(extension) < 0) { 
        alert("Debe cargar un archivo Excel con extensión .xls o .xlsx");
        return false;
    }
  });

});
</script>