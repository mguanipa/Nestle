
<div class="container">
    <nav class="navbar navbar-expand-lg navbar-light">
        <a class="navbar-brand mx-center" href="<?php echo $this->Url->build('/'); ?>">
            <?php echo $this->Html->image("/frontend/img/logo.png"); ?>
            
        </a>
         <a style="color: #00a5da;" href="<?php echo $this->Url->build('/'); ?>"><i class="fa fa-sign-out" aria-hidden="true"></i></a>
    </nav>
    <form action="<?php echo $this->Url->build('/users/forgot_password'); ?>" enctype='multipart/form-data' method="post">
        <div class="row">

            <div class="col p-5 second-col-login">
                <div class="row">
                    <div class="col-12 col-md-6 d-block mx-auto" style="top: 39%;transform: translate(-50%);position: absolute;left: 50%;">
                        <div class="form-group">
                            <label for="">Email:</label>
                            <input name="email" class="form-control" id="" type="email">
                        </div>

                         <?= $this->Flash->render() ?>

                        <button class="btn btn-primary mx-auto mt-5" type="submit">Verificar</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>