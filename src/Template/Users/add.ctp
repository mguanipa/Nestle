<?php
/**
  * @var \App\View\AppView $this

<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __('Add User') ?></legend>
        <?php
            echo $this->Form->input('first_name');
            echo $this->Form->input('last_name');
            echo $this->Form->input('email');
            echo $this->Form->input('password');
            echo $this->Form->input('role');
            echo $this->Form->input('active');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
  */
?>
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Lista de Usuarios <small><strong></strong></small></h3>
              </div>
            </div>
          </div>
            <div class="col-md-12 col-xs-8">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Agregar Usuarios<small></small></h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    
                    <?= $this->Form->create($user, ['class' => 'form-horizontal form-label-left input_mask']) ?>
                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    
                        <?php
                        echo $this->Form->input('first_name', ['class' => 'form-control','id'=>'inputSuccess2','placeholder'=>'Nombre ' ]);
                        ?>
                      </div>

                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        
                        <?php
                        echo $this->Form->input('last_name', ['class' => 'form-control','id'=>'inputSuccess3','placeholder'=>'Apellido' ]);
                        ?>
                      </div>

                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        
                        <?php
                        echo $this->Form->input('email', ['class' => 'form-control','id'=>'inputSuccess4','placeholder'=>'Email' ]);
                        ?>
                      </div>

                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        
                        <?php
                        echo $this->Form->input('Username', ['class' => 'form-control','id'=>'inputSuccess5','placeholder'=>'Nombre de Usuario' ]);
                        ?>
                      </div>

                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        
                        <?php
                        echo $this->Form->input('password', ['class' => 'form-control','id'=>'inputSuccess6','placeholder'=>'password' ]);
                        ?>
                      </div>

                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        
                       <label for="ex3">Rol</label>
                                  <select name="role" id="heard" class="form-control" >
                                     <option value="">Seleccione</option>
                                        <option value="admin">admin</option>
                                        <option value="user">talento</option>
                                        <option value="empresa">empresa</option>
                                  </select>
                      </div>

                       

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                        
                        <?php
                        echo $this->Html->link('Regresar','/users   ',['class' => 'btn btn-success']);
                        ?>

                          <?= $this->Form->button(__('Guardar'), ['class' => 'btn btn-success']) ?>
                        </div>
                      </div>

                    <?= $this->Form->end() ?>
                  </div>
                </div>

                    
                </div>


              </div>

              </div>