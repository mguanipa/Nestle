<div class="container">
<!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-light">
        <a class="navbar-brand mx-center" href="<?php echo $this->Url->build('/'); ?>">
            <?php echo $this->Html->image("/frontend/img/logo.png"); ?>
            
        </a>
         <a style="color: #00a5da;" href="<?php echo $this->Url->build('/'); ?>"><i class="fa fa-sign-out" aria-hidden="true"></i></a>
    </nav>

    <form action="<?php echo $this->Url->build('/users/Registro_Talento'); ?>" enctype='multipart/form-data' method="post">
        <div class="row">

            <div class="col p-5 align-self-stretch d-block second-col-login">
             <h4 class="text-center" style="color:white;margin-bottom:50px; ">
        Si quieres continuar viendo nuestros entrenamientos, <br>completa el formulario de registro
    </h4>
                <div class="row">

                    <div class="col-12 col-md-6 mx-auto">
                        <div class="form-group">
                            <label for="">Usuario:</label> <input name="Username" class="form-control" id="" type="username" required>
                        </div>
                        <div class="form-group">
                            <label for="">Correo:</label> <input name="email" class="form-control" id="" type="email" required>
                        </div>
                        <div class="form-group">
                            <label for="">Contraseña:</label> <input name=" password" class="form-control" id="password" type="password" required>
                        </div>
                        <div class="form-group">
                            <label for="">Confirmar Contraseña:</label> <input class="form-control" id="confirm_password" type="password" required>
                        </div>
                        <?= $this->Flash->render() ?>
                        <button class="btn btn-primary mx-auto mt-5" type="submit">Registro</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>





