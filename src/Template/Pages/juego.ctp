<div class="container">

    <div class="row align-items-center justify-content-between">

        <div class="col-6 col-md-4">
        <?php echo $this->Html->image("/frontend/img/juego/talento-con-gusto-logo.png", ['class' => 'mt-2 img-fluid']); ?>
        <a href="<?php echo $this->Url->build('/'); ?>"><?php echo $this->Html->image("/frontend/img/juego/log-nestle.png", ['class' => 'img-fluid mt-2 d-md-none',]); ?></a>
        </div>
        <div class="col-6 col-md-4">
            <span class="level">Nivel <?php echo $level ;?></span>
        </div>
        <div class="col-12 col-md-4">
        <a style="color: #00a5da;" href="<?php echo $this->Url->build('/'); ?>"><i class="fa fa-sign-out" aria-hidden="true"></i></a>
        <a href="<?php echo $this->Url->build('/'); ?>"><?php echo $this->Html->image("/frontend/img/juego/log-nestle.png", ['class' => 'img-fluid logo-nestle',]); ?></a>
        </div>

    </div>

    <div class="row justify-content-center mt-4 mb-4">
        <div class="col col-md-4">
            <div class="row">
                <div class="col">
                    <div class="grid">
                        <?php 
                            for ($i=1; $i < 5 ; $i++) { ?>
                                <div class="grid-item">
                                    <a  data-lightbox="<?php echo $level ?>" href="<?php echo $this->Url->build("/frontend/img/juego/niveles/".$level."/".$i.".jpg",true); ?>"><?php echo $this->Html->image("/frontend/img/juego/niveles/".$level."/".$i.".jpg", ['class' => 'img-fluid']); ?></a>
                                </div>  
                         <?php } ?>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="wrapper">
        <div id="hold">
        </div>
        <div id="buttons">
        </div>
        <div class="hombre">
            <?php echo $this->Html->image("/frontend/img/juego/hombre-neutral.png", ['class' => 'img-fluid persona neutral', 'style'=> 'display:block']); ?>
            <?php echo $this->Html->image("/frontend/img/juego/hombre-feliz.png", ['class' => 'img-fluid persona feliz', 'style'=> 'display:none']); ?>
            <?php echo $this->Html->image("/frontend/img/juego/hombre-triste.png", ['class' => 'img-fluid persona triste', 'style'=> 'display:none']); ?>
        </div>
        <div class="mujer">
            <?php echo $this->Html->image("/frontend/img/juego/mujer-neutral.png", ['class' => 'img-fluid persona neutral', 'style'=> 'display:block']); ?>
            <?php echo $this->Html->image("/frontend/img/juego/mujer-feliz.png", ['class' => 'img-fluid persona feliz', 'style'=> 'display:none']); ?>
            <?php echo $this->Html->image("/frontend/img/juego/mujer-triste.png", ['class' => 'img-fluid persona triste', 'style'=> 'display:none']); ?>

            <?php echo $this->Html->image("/frontend/img/juego/hint.png", ['class' => 'img-fluid persona hint']); ?>
            
            <span class="persona nro-pista">2</span>
        </div>
    </div>
    <div class="hint-mobile">
        <?php echo $this->Html->image("/frontend/img/juego/hint.png", ['class' => 'img-fluid hint']); ?>
        <span class="nro-pista">2</span>
    </div>
    <!-- <a href="#inline" data-lity>Inline</a>
    <div id="inline" style="background:#fff" class="lity-hide">
        <div id="player"></div>
    </div> -->
</div>

<span style="display: none" id="word"><?php echo $palabras; ?></span>
<span style="display: none" id="level"><?php echo $level ;?></span>

