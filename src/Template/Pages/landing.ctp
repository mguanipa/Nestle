<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=262312817134179";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class="container">

    <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand mx-center" href="#">
         <?php echo $this->Html->image("/frontend/img/logo.png"); ?>

            </a>
            <style type="text/css">
                .navbar-textfloat-lg-left{
                font-size: 19px;
                margin-left: 10px;
                display: block;
                margin-bottom: 0px!important;
                }
            </style>
            <div class="navbar-collapse flex-row-reverse" id="navbarNavAltMarkup">
  
                <?php if($this->request->session()->read('Auth.User.id')){ 
                   if( empty( $name = $this->request->session()->read('Auth.User.first_name') )){
                    $name = "talento";
                   }
                ?>
                    <span class="navbar-textfloat-lg-left">
                         Hola <b><?php echo($name); ?></b>
                    </span>
             
                  <span class="navbar-textfloat-lg-right">
                 <a style="display: block" href="<?php echo $this->Url->build('/users/logout', true); ?>">
                    <?php echo $this->Html->image("/frontend/img/landing/cerrar-sesion.png", ['class' => 'img-fluid mt-4 mt-lg-0 mb-4 mb-lg-0 mx-center login-now']); ?>
                    </a>
                    </span>

                <?php }else{ ?>
                <span class="navbar-textfloat-lg-right">
                <a style="display: block" href="<?php echo $this->Url->build('/users/login', true); ?>">
                    <?php echo $this->Html->image("/frontend/img/landing/iniciar-sesion.png", ['class' => 'img-fluid mt-4 mt-lg-0 mb-4 mb-lg-0 mx-center login-now']); ?>
                    </a>
                   </span>
 
                 <?php }   ?>

                
                    
                <div class="navbar-nav justify-content-between justify-content-lg-start flex-row float-lg-right">
                    <a class="nav-item nav-link active" href="#second-section">Talento con gusto</a>
                    <a class="nav-item nav-link" href="<?php echo $this->Url->build('/blog/', true); ?>">Noticias</a>
                    <a class="nav-item nav-link" href="#fifth-section">Contáctanos</a>
                </div>
            </div>
        </nav>

    </div>

    <!-- Main container -->

    <div class="container" style="position: relative;">
        <div id="first-section" class="row align-items-start align-items-md-center first-section">
         <?php echo $this->Html->image("/frontend/img/landing/banner-1.jpg", ['class' => 'img-fluid img-bg-1']); ?>
         <?php echo $this->Html->image("/frontend/img/landing/banner-1-mobile.png", ['class' => 'img-fluid img-bg-1-mobile']); ?>

            <div class="col-12 col-md-6 p-1 bg-first-section align-items-md-center d-flex">
                <p class="text-first-section">Contribuimos en la preparación de la próxima generación de trabajadores para Venezuela</p>
            </div>
            <div class="col-9 col-md-4 p-0 mx-auto text-center text-md-left">
                <a data-lity href="https://youtu.be/vDD1ACDRHn4&rel=0&showinfo=0&autoplay=1">
                    <?php echo $this->Html->image("/frontend/img/landing/video.png", ['class' => 'video-play img-fluid']); ?>
                </a>
            </div>
            <div class="col-12 col-md-2">
            </div>
            <a href="#second-section" class="arrow-section arrow-section-1"></a>
        </div>
        <div id="second-section" class="row align-items-lg-center align-items-start  second-section">
         <?php echo $this->Html->image("/frontend/img/landing/banner-2.jpg", ['class' => 'img-fluid img-bg-2']); ?>
         <?php echo $this->Html->image("/frontend/img/landing/banner-2-mobile.jpg", ['class' => 'img-fluid img-bg-2']); ?>

            <div class="col-12 col-md-4 bg-second-section">
                <p class="text-second-section text-md-right text-center font-weight-bold">Talento con gusto</p>
                <p class="text-second-section text-md-right text-center">Completa los <strong>e-learning</strong> y obtén insumos, formación profesional y la oportunidad de subir tu CV que será visto por Nestlé y sus aliados.</p>
            </div>
            <div class="col-12 col-md-4 align-self-stretch align-items-center d-flex">
                <p class="text-second-section-2 text-md-left text-center">Iniciativa por los jóvenes te ofrece un espacio donde aprenderás habilidades para conseguir tu primer empleo a la vez que conoces los procesos de producción y distribución que Nestlé® lleva a cabo para que disfrutes de sus productos.</p>
            </div>
            <div class="col-12 col-md-4">

            </div>
            <div class="col-12 text-center p-4">
                <a href="#third-section" class="btn-second-section mx-auto">Empieza tu ruta hacia el éxito y descarga recursos</a>
            </div>
            <a href="#third-section" class="arrow-section arrow-section-2"></a>
        </div>
        <div id="third-section" class="row align-items-center third-section">
            <?php echo $this->Html->image("/frontend/img/landing/banner-3.png", ['class' => 'img-fluid img-bg-3']); ?>
            <?php echo $this->Html->image("/frontend/img/landing/banner-3-mobile.png", ['class' => 'img-fluid img-bg-3-mobile']); ?>
            <?php echo $this->Html->image("/frontend/img/landing/banner-3.png", ['class' => 'img-fluid d-none d-md-flex']); ?>
            <?php echo $this->Html->image("/frontend/img/landing/banner-3-mobile.png", ['class' => 'img-fluid d-md-none', 'style="width:100%;"']); ?>
            <?php $step = $this->request->session()->read('Auth.User.step') ;

                if (empty($step)) {
                    $step = 0;
                }
             ?>
            <a href="<?php echo $this->Url->build('/pages/juego'); ?>" id="steps-1" class="steps steps-1 clickable <?php if($step >= 1){ echo "active";}?>"></a>

            <?php
                $id_user = $this->request->session()->read('Auth.User.id');

                if (!empty($id_user)) {

              

             ?>
             <!-- con session -->
            <a href="<?php echo $this->Url->build('/webroot/recompensa/modulo1.pdf'); ?>" class="download-reward download-reward-1 <?php  if($step >= 1){ echo ' active ';}  ?>" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a>
            <a href="<?php echo $this->Url->build('/webroot/recompensa/modulo2-1.pdf'); ?>" class="download-reward download-reward-2 <?php if($step >= 2){ echo ' active ';} ?> " target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a>
            <a href="https://www.youtube.com/watch?v=Ixrwr_Qm5Is&rel=0&showinfo=0&autoplay=1" data-lity class="download-reward download-reward-2-1 <?php if($step >= 2){ echo ' active ';} ?>"><i class="fa fa-play" aria-hidden="true"></i></a>
            <a href="<?php echo $this->Url->build('/webroot/recompensa/modulo3.pdf'); ?>" class="download-reward download-reward-3 <?php if($step >= 3){ echo ' active ';} ?> " target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a>
            <a href="https://www.youtube.com/watch?v=B3SPnxMBkAo" data-lity class="download-reward download-reward-3-1 <?php if($step >= 3){ echo ' active ';} ?>" style="margin-top: -4%; margin-left: 32%;"><i class="fa fa-play" aria-hidden="true"></i></a>
            <a href="<?php echo $this->Url->build('/webroot/recompensa/modulo4.pdf'); ?>" class="download-reward download-reward-4  <?php if($step >= 4){ echo ' active ';} ?>  " target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a>
         
            <a href="#steps-2" data-step="2" class=" steps steps-2 clickable <?php  if($step == 2){ echo 'active clickable';}?>"></a>
            <a href="#steps-3" data-step="3" class=" steps steps-3 clickable <?php  if($step == 3){ echo 'active clickable';}?>"></a>
            <a href="#steps-4" data-step="4" class=" steps steps-4 clickable <?php if($step == 4){ echo 'active clickable';}?>"></a>
            <a href="#steps-5" data-step="5" class=" steps steps-5 clickable <?php if($step == 5){ echo 'active clickable';}?>"></a>
            <a href="#steps-6" <?php if($step == 6 ){echo'style="display:none ;"';}?>  data-step="6" class=" steps steps-6 <?php if($step >= 3){ echo 'active clickable';}elseif ($step == 3) {echo 'clickable';  };?>"></a>
            <a href="<?php echo $this->Url->build('/users/UpdateTalento/'); ?>" <?php if($step == 6 ){echo'style="display:block ;"';}else{echo 'style="display: none;"';}?> class="steps cargar-cv clickable"></a>


            <?php } else{ ?>
            <!-- sin sesion -->

            <a href="#" data-step="2" class="steps steps-2 clickable"></a>
            <a href="#" data-step="3" class="steps steps-3 clickable"></a>
            <a href="#" data-step="4" class="steps steps-4 clickable"></a>
            <a href="#" data-step="5" class="steps steps-5 clickable"></a>
            <a href="#" data-step="6" class="steps steps-6"></a>

           <?php } ?>

           <!-- Your share button code -->
           <div class="fb-share-button" data-href="<?php echo $this->Url->build('/', true)?>" data-layout="button_count" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fnestlecontigo.com.ve/youth%2F&amp;src=sdkpreparse">Share</a></div>

            <a href="#news" class="arrow-section arrow-section-3"></a>
        </div>
        <div id="fourth-section" class="row align-items-center fourth-section">
            <?php echo $this->Html->image("/frontend/img/landing/banner-4.jpg", ['class' => 'img-fluid img-bg-4']); ?>
            <div class="col-md-1"></div>
            <div class="col-12 col-md-4 bg-fourth-section align-self-stretch align-items-center d-flex px-5">
                <div class="text-center">
                    <h1 class="font-weight-bold" id="news">Noticias</h1>
                    <p>Entérate de más sobre la Iniciativa por los Jóvenes en Venezuela</p>
                    <a href="<?php echo $this->Url->build('/blog/'); ?>" class="btn-fourth-section mx-auto d-none d-md-block">Conoce Más</a>                    
                </div>
            </div>
            <div class="col-12 col-md-4 align-self-stretch align-items-center d-flex">
                <p class="text-second-section-4 text-md-left text-center d-block">Queremos contarte todo lo que hacemos día a día en Venezuela y en el mundo para preparar y desarrollar a nuestros jóvenes. Te invitamos a conocer más sobre nuestros eventos, talleres, charlas y nuevos lanzamientos</p>
            </div>
            <div class="col-12 align-self-stretch align-items-center d-md-none d-flex">
                <a href="#" class="btn-fourth-section mx-auto d-block d-md-none">Conoce más sobre nosotros</a>
            </div>
            <a href="#contact-us" class="arrow-section arrow-section-4"></a>
        </div>
        <div id="fifth-section" class="row align-items-lg-center align-items-start fifth-section">
            <?php echo $this->Html->image("/frontend/img/landing/banner-5.png", ['class' => 'img-fluid img-bg-5']); ?>
            <?php echo $this->Html->image("/frontend/img/landing/banner-5-mobile.jpg", ['class' => 'img-fluid img-bg-5-mobile']); ?>
            <a href="#contact-us" class="arrow-section arrow-section-4"></a>
            <div class="col-12 col-md-5 bg-fifth-section text-center text-md-left">
                <h1 class="font-weight-bold" id="contact-us">Contáctanos</h1>
                <p>Queremos responder todas tus inquietudes</p>
                <table class="d-none d-md-flex">
                    <tr>
                        <th rowspan="2">
                        <?php echo $this->Html->image("/frontend/img/landing/phone-icon.png", ['style' => 'margin-left:-30px']); ?>
                        <th><a href="mailto:Servicio.Consumidor@ve.nestle.com">Servicio.Consumidor@ve.nestle.com</a></th>
                    </tr>
                        <tr>
                        <td>(0212) 0800 Nestlé 1 - (0800 637853 1)</td>
                    </tr>
                </table>
            </div>
            <div class="col-12 col-lg-7 d-md-none d-lg-flex"></div>
            <div class="col-12 col-lg-5 d-md-none d-lg-flex"></div>
            <div class="col-12 col-md-7 col-lg-7">
                <div class="second-fifth-section">
                    <table class="d-flex d-md-none">
                        <tbody class="mx-auto text-center">
                            <tr>
                                <th class="text-center">
                                <?php echo $this->Html->image("/frontend/img/landing/phone-icon.png"); ?>
                                </th>
                            </tr>
                            <tr>
                                <th style="text-align: center;"><a href="mailto:Servicio.Consumidor@ve.nestle.com">Servicio.Consumidor@ve.nestle.com</a></th>
                            </tr>
                            <tr>
                                <td>(0212) 0800 Nestlé 1 - (0800 637853 1)</td>
                            </tr>
                        </tbody>
                    </table>
                    <p class="social-media text-center text-md-left">
                    <a href="https://www.instagram.com/nestlecontigo" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                    <a href=" https://www.facebook.com/NestleContigo" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    <a href="https://twitter.com/NestleContigo"  target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                    ¡Síguenos!</p>
                </div>
            </div>
        </div>
        <a href="#" class="toTop"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
    </div>

    <?php echo $this->Html->image("/frontend/img/landing/paso-1-hover.png", ['class' => 'd-none']); ?>
    <?php echo $this->Html->image("/frontend/img/landing/paso-2-hover.png", ['class' => 'd-none']); ?>
    <?php echo $this->Html->image("/frontend/img/landing/paso-3-hover.png", ['class' => 'd-none']); ?>
    <?php echo $this->Html->image("/frontend/img/landing/paso-4-hover.png", ['class' => 'd-none']); ?>
    <?php echo $this->Html->image("/frontend/img/landing/paso-5-hover.png", ['class' => 'd-none']); ?>
    <?php echo $this->Html->image("/frontend/img/landing/paso-6-hover.png", ['class' => 'd-none']); ?>
    <?php echo $this->Html->image("/frontend/img/landing/cargar-cv-hover.png", ['class' => 'd-none']); ?>
