
    <div class="container p-0">
        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-light p-0 pl-lg-4 pt-lg-2 pb-lg-2 pt-2 pb-2 pl-3">
            <a class="navbar-brand mx-center" href="#">
                <?php echo $this->Html->image("/frontend/img/logo.png"); ?>
            </a>
            
            <div class="navbar-collapse flex-row-reverse" id="navbarNavAltMarkup">
                <span class="navbar-text float-right">
                    <a class="d-block text-right mt-3 mt-lg-0" href="<?php echo $this->Url->build('/users/talentos'); ?>">
                    <?php echo $this->Html->image("/frontend/img/empresas/ver-cv.png", ['class' => 'img-fluid']); ?>
                    </a>
                </span>
            </div>
        </nav>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 col-lg-4 sections section-1">
                <div class="img-container">
                <?php echo $this->Html->image("/frontend/img/empresas/icon-1.png", ['class' => 'd-block mx-auto icons']); ?>
               
                </div>
                <div class="d-block mx-auto sections-text">
                    <h3>Cómo hacer un CV atractivo</h3>
                    <p>¿Ya sabes qué es lo más importante al hacer tu CV? Descarga ahora ésta infografía y ¡Enseña a tus amigos!</p>
                   
                    <a href="https://www.youtube.com/watch?v=LbjcB0X3Ig4&rel=0&showinfo=0&autoplay=1" data-lity><i class="fa fa-download" aria-hidden="true"></i> Video</a>
                    <a href="<?php echo $this->Url->build('/pages/juego'); ?>"><i class="fa fa-download" aria-hidden="true"></i> Juego</a>
                    <a href="<?php echo $this->Url->build('/webroot/recompensa/modulo1.pdf'); ?>" download><i class="fa fa-download" aria-hidden="true"></i> Infografía</a>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 sections section-2">
                <div class="img-container">
                <?php echo $this->Html->image("/frontend/img/empresas/icon-2.png", ['class' => 'd-block mx-auto icons']); ?>
                
                </div>
                <div class="d-block mx-auto sections-text">
                    <h3>El primer cara a cara</h3>
                    <p>¡Conoce mucho más de Nestlé y de lo que hacemos! 
                        <br> <br>
                          <a href="https://www.youtube.com/watch?v=Ixrwr_Qm5Is&rel=0&showinfo=0&autoplay=1" data-lity style=" font-size: 16px;"><i class="fa fa-download" aria-hidden="true"></i> Video</a>
                        <br>
                        Ya que sabes lo que debes hacer en tu entrevista de trabajo, descarga esta infografía para que sepas cómo prepararte para las pruebas psicotécnicas (infografía adjunta)
                    </p>
                  
                    <a href="<?php echo $this->Url->build('/webroot/recompensa/modulo2-1.pdf'); ?>" download><i class="fa fa-download" aria-hidden="true"></i> Infografía</a>
                    <a href="https://www.youtube.com/watch?v=CZnvCpryQX8&rel=0&showinfo=0&autoplay=1" data-lity style=" font-size: 16px;"><i class="fa fa-download" aria-hidden="true"></i> Video</a>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 sections section-3">
                <?php echo $this->Html->image("/frontend/img/empresas/icon-3.png", ['class' => 'd-block mx-auto icons']); ?>
                    
                    <div class="d-block mx-auto sections-text">
                        <h3>Potencia tu imagen personal con este e-learning</h3>
                        <p>  En Iniciativa Por Los Jóvenes estamos constantemente formando a nuevos talentos. ¡Ve el video de nuestro evento Conexión al Éxito! Y coméntanos qué te parece
                        <br>
                         <a href="https://www.youtube.com/watch?v=_xEEOyehYZY&rel=0&showinfo=0&autoplay=1" data-lity style=" font-size: 16px;"><i class="fa fa-download" aria-hidden="true"></i> Video</a>
                        <br>
                        Haz click en descargar para que tengas información sobre bolsas de empleo que te pueden ayudas a encontrar el trabajo de tus sueños
                        </p>
                       
                        <a href="<?php echo $this->Url->build('/webroot/recompensa/modulo3.pdf'); ?>" download><i class="fa fa-download" aria-hidden="true"></i> Infografía</a>

                    </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 sections section-4">
                <?php echo $this->Html->image("/frontend/img/empresas/icon-4.png", ['class' => 'd-block mx-auto icons']); ?>
                <div class="d-block mx-auto sections-text">
                    <h3>Taller de comunicación asertiva/ liderazgo/ feedback</h3>
                    <p> ¿Crees en tu potencial? Queremos contarte sobre uno de nuestros programas de formación en Iniciativa Por Los Jóvenes.
                    <br>
                <a href="https://www.youtube.com/watch?v=bIx1Vez0l6k&rel=0&showinfo=0&autoplay=1" data-lity style=" font-size: 16px;"><i class="fa fa-download" aria-hidden="true"></i> Video</a>
                    <br>
                     Haz click en descargar y conoce más sobre nuestro programa de Practicantes</p>
                    
                    <a href="<?php echo $this->Url->build('/webroot/recompensa/modulo4.pdf'); ?>" download><i class="fa fa-download" aria-hidden="true"></i> Infografía</a>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 sections section-5">
               <?php echo $this->Html->image("/frontend/img/empresas/icon-5.png", ['class' => 'd-block mx-auto icons']); ?>
                <div class="d-block mx-auto sections-text">
                    <h3>Conoce los componentes de las ofertas salariales</h3>
                    <p>Si eres mayor de edad, y quieres conocer más de cerca a Nestlé está atento a nuestras redes @NestleContigo porque estaremos seleccionando a jóvenes como tú para que nos acompañen en este recorrido</p>
                    <a href="https://www.youtube.com/watch?v=PihVBeLFzkg&rel=0&showinfo=0&autoplay=1" data-lity ><i class="fa fa-download" aria-hidden="true"></i> Video</a>
              
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 sections section-6">
                <?php echo $this->Html->image("/frontend/img/empresas/icon-6.png", ['class' => 'd-block mx-auto icons']); ?>
                <div class="d-block mx-auto sections-text">
                    <h3>Para vender tu chocolate es importante manejar tus finanzas</h3>
                    <p>Formulario de registro</p>
                    <a href="https://www.youtube.com/watch?v=o72fc0K52fg&rel=0&showinfo=0&autoplay=1" data-lity><i class="fa fa-download" aria-hidden="true"></i> Video</a>
                   
                </div>
            </div>
        </div>
    </div>

    <!-- Main container -->
