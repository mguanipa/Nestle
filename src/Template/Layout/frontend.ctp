<!DOCTYPE html>
<html lang="es">

<head>
    <title>Nestle - Iniciativa Por Los Jóvenes</title>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes">
    <meta property="og:url"           content="<?php echo $this->Url->build('/', true); ?>" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="6 pasos que te acercarán a tu trabajo ideal" />
    <meta property="og:description"   content="Contribuimos en la preparación de la próxima generación de trabajadores para Venezuela. Iniciativa por los jóvenes te ofrece un espacio donde aprenderás habilidades para conseguir tu primer empleo." />
    <meta property="og:image"         content="<?php echo $this->Url->build('/frontend/img/share-facebook.jpeg', true); ?>" />

    <?= $this->Html->script('/frontend/js/pace.min.js') ?>
    <?= $this->Html->css('/frontend/css/pace-theme-fill-left.css') ?>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">

    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-29119472-4', 'auto');
    ga('send', 'pageview');

    </script>

    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,700&amp;subset=latin-ext" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/lity/2.3.0/lity.min.css">
    <?= $this->Html->css('/frontend/css/custom.css') ?>

    <style id="antiClickjack">body{display:none !important;}</style>
</head>

<body>
    <?= $this->fetch('content') ?>
    <!-- Optional JavaScript -->
    <script type="text/javascript">
        if (self === top) {
            var antiClickjack = document.getElementById("antiClickjack");
            antiClickjack.parentNode.removeChild(antiClickjack);
        } else {
            top.location = self.location;
        }
    </script>
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lity/2.3.0/lity.min.js"></script>

    <script>
        var $buoop = {vs:{i:11,f:-6,o:-6,s:8,c:-6},unsecure:true,api:4, reminder: 0, noclose:true, text_es: "<b>Tu navegador web {brow_name} no está soportado.</b> Actualiza tu navegador para tener más seguridad y comodidad y tener la mejor experiencia en este sitio. <a{up_but}>Actualizar navegador</a>"};
        function $buo_f(){
         var e = document.createElement("script");
         e.src = "//browser-update.org/update.min.js";
         document.body.appendChild(e);
        };
        try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
        catch(e){window.attachEvent("onload", $buo_f)}
    </script>

    <?= $this->Html->script('/frontend/js/custom.js') ?>
    <?php $step =  $this->request->session()->read('Auth.User.first') ;
    
    if (isset($step)   ){
        if($step == 1 or $step == 0 ){
        ?>


    <script type="text/javascript">
        lity('<div class="container"><div class="row agradecimiento"><div class="col-12 align-self-center text-center"><img class="img-fluid" src="img/registro/logo-small.png"><h1>Gracias<br> por Registrarte</h1><h3>En este portal de Iniciativa por los jóvenes <strong>encontrarás e-learning o capacitaciones que te brindarán la oportunidad de subir tu CV</strong> y que Nestlé y sus aliados puedan contactarte</h3></div></div></div>')
    </script>
    <?php }

     $this->request->session()->write( 'Auth.User.first',2);
    }
    ?>
</body>

</html>