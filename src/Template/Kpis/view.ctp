<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Kpi'), ['action' => 'edit', $kpi->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Kpi'), ['action' => 'delete', $kpi->id], ['confirm' => __('Are you sure you want to delete # {0}?', $kpi->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Kpis'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Kpi'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="kpis view large-9 medium-8 columns content">
    <h3><?= h($kpi->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($kpi->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Dowload') ?></th>
            <td><?= $this->Number->format($kpi->dowload) ?></td>
        </tr>
    </table>
</div>
