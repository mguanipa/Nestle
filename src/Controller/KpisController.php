<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Kpis Controller
 *
 * @property \App\Model\Table\KpisTable $Kpis
 */
class KpisController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $kpis = $this->paginate($this->Kpis);

        $this->set(compact('kpis'));
        $this->set('_serialize', ['kpis']);
    }

    /**
     * View method
     *
     * @param string|null $id Kpi id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $kpi = $this->Kpis->get($id, [
            'contain' => []
        ]);

        $this->set('kpi', $kpi);
        $this->set('_serialize', ['kpi']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $kpi = $this->Kpis->newEntity();
        if ($this->request->is('post')) {
            $kpi = $this->Kpis->patchEntity($kpi, $this->request->data);
            if ($this->Kpis->save($kpi)) {
                $this->Flash->success(__('The kpi has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The kpi could not be saved. Please, try again.'));
        }
        $this->set(compact('kpi'));
        $this->set('_serialize', ['kpi']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Kpi id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit()
    {
        $this->autoRender = false;
       
        if ($this->request->is(['post'])) {

            $id = $this->request->data['id'];   
            $kpi2 = $this->Kpis->get($id, [
            'contain' => []
            ]);

            $valor = $kpi2->dowload + 1;

            $kpi = $this->Kpis->get($id, [
            'contain' => []
            ]);

            $this->request->data['dowload'] = $valor;

            $kpi = $this->Kpis->patchEntity($kpi, $this->request->data);
            if ($this->Kpis->save($kpi)) {
                echo  $valor;
            }
            
        }
     
    }

    /**
     * Delete method
     *
     * @param string|null $id Kpi id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $kpi = $this->Kpis->get($id);
        if ($this->Kpis->delete($kpi)) {
            $this->Flash->success(__('The kpi has been deleted.'));
        } else {
            $this->Flash->error(__('The kpi could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
