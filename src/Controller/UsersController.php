<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Mailer\Email;
include '../vendor/Classes/PHPExcel.php';
include '../vendor/Classes/PHPExcel/IOFactory.php';
//require_once ('/vendor/Classes/PHPExcel/IOFactory.php');


/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */


class UsersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */

   var $paginate = array( 'limit' => 1000);

    public function autorizadmin($role = null)
    {
        if ($role == "admin"){
            return 1;
        } else{
            return $this->redirect('/Users/login');
        }
    }

    public function adminempresa($role = null){
       if ($role == "admin" or $role == "empresa" ){
            return 1;
        }else{
            return $this->redirect('/Users/login');
        }
    }

    public function adminetalento($role = null){
       if ($role == "admin" or $role == "user" ){
            return 1;
        }else{
            return $this->redirect('/Users/login');
        }

    }

    public function index(){
        $role =  $this->request->session()->read('Auth.User.role') ;
        $this->autorizadmin($role);
        $users = $this->paginate($this->Users,[ 'conditions' => [ 'role !=' => 'user' ]]);
        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    public function login(){
        $this->viewBuilder()->layout( "LayoutLogin");

        if ($this->Auth->user()) {
            $role =  $this->request->session()->read('Auth.User.role') ;
            switch ($role) {
                case 'empresa':
                return $this->redirect('/Pages/empresa');
                break;
                case 'user':                    
                return $this->redirect('/Pages/landing');
                break;
                default:
                return $this->redirect('/Users/index');
                break;
            }
        }else{
            if ($this->request->is('post')) {
                $user = $this->Auth->identify();
                if ($user) {
                    $this->Auth->setUser($user);
                    $role =  $this->request->session()->read('Auth.User.role') ;
                    switch ($role) {
                        case 'empresa':
                        return $this->redirect('/Pages/empresa');
                        break;
                        case 'user':                    
                        return $this->redirect('/Pages/landing');
                        break;

                        default:

                        return $this->redirect('/Users/index');
                        break;
                    }
                }
                $this->Flash->error(__('Nombre de usuario o clave incorrectos'));
            }
        }
    }
     

    public function logout(){
        $this->autoRender = false;
        return $this->redirect($this->Auth->logout());
    }

  
    public function view($id = null){
        $role =  $this->request->session()->read('Auth.User.role') ;
        $this->autorizadmin($role);

        $user = $this->Users->get($id, [
            'contain' => []
        ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $role =  $this->request->session()->read('Auth.User.role') ;
        $this->autorizadmin($role);

        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
          $this->request->data['active'] = 1;
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $role =  $this->request->session()->read('Auth.User.role') ;
        $this->autorizadmin($role);

        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $role =  $this->request->session()->read('Auth.User.role') ;
        $this->autorizadmin($role);

        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function registro(){

        $this->viewBuilder()->layout( "LayoutLogin");
        $id_session =  $this->request->session()->read('Auth.User.id') ;

        if (empty( $id_session )) {
            $user = $this->Users->newEntity();
            if ($this->request->is('post')) {
                
                $this->request->data['role'] = 'user';
                $this->request->data['active'] = 1;
                $this->request->data['step'] = 0;
                $this->request->data['url_cv'] = '';


                $user = $this->Users->patchEntity($user, $this->request->data);

                if ($result=$this->Users->save($user)) {

                    $authUser = $this->Users->get($result->id)->toArray();

                    // Log user in using Auth
                    $this->Auth->setUser($authUser);
                    $this->request->session()->write( 'Auth.User.first',1);
                    $role =  $this->request->session()->read('Auth.User.role') ;
                    switch ($role) {
                        case 'empresa':
                        return $this->redirect('/Pages/empresa');
                        break;
                        case 'user':                    
                        return $this->redirect('/Pages/landing');
                        break;
                        default:
                        return $this->redirect('/Users/index');
                        break;
                    }              
                }
                $this->Flash->error(__('Su registro no pudo ser realizado. Por favor intente nuevamente'));
            }
            $this->set(compact('user'));
            $this->set('_serialize', ['user']);
        }else{
            return $this->redirect('/users/login');
        }
    }


    public function RegistroTalento(){

        $id_session =  $this->request->session()->read('Auth.User.id') ;


        if (empty( $id_session )) {
            $this->viewBuilder()->layout( "LayoutLogin");
            $user = $this->Users->newEntity();
            if ($this->request->is('post')) {

                $this->request->data['role'] = 'user';
                $this->request->data['active'] = 1;
                $this->request->data['step'] = 1;
                $user = $this->Users->patchEntity($user, $this->request->data);
                // var_dump($user); 

                if ($result=$this->Users->save($user)) {

                    $authUser = $this->Users->get($result->id)->toArray();

                    // Log user in using Auth
                    $this->Auth->setUser($authUser);
                     $this->request->session()->write( 'Auth.User.first',1);
                    $role =  $this->request->session()->read('Auth.User.role') ;
                    switch ($role) {
                        case 'empresa':
                        return $this->redirect('/Pages/empresa');
                        break;
                        case 'user':                    
                        return $this->redirect('/Pages/landing');
                        break;

                        default:

                        return $this->redirect('/Users/index');
                        break;
                    }              

                }
                $this->Flash->error(__('Su registro no pudo ser realizado. Por favor intente nuevamente'));
            }

        }else{
            return $this->redirect('/users/login');
        }

    }

    public function UpdateTalento(){
    
        $id =  $this->request->session()->read('Auth.User.id') ;
        $this->viewBuilder()->layout( "LayoutLogin");


        if (!empty( $id )) {

            $user = $this->Users->get($id, [
                'contain' => []
            ]);

            if ($this->request->is('post')) {
                $this->request->session()->write( 'Auth.User.step',6);
                $name = $this->request->data['cv_file']['name'];
                $filesize = $this->request->data['cv_file']["size"];
                $file_ext = substr($name, strripos($name, '.'));
                $allowed_file_types = array('.doc','.docx','.pdf');  



                $tmp_name = $this->request->data['cv_file']['tmp_name'];
                $filename = WWW_ROOT."file/cv/".preg_replace('[\s+]','', "id-".$id.$file_ext);
                move_uploaded_file($tmp_name,$filename);

                chmod($filename , 0750); 
                $this->request->data['step'] = 6;
                $url_cv = "../webroot/file/cv/".preg_replace('[\s+]','', "id-".$id.$file_ext);
                $this->request->data['url_cv'] = $url_cv;


                $user = $this->Users->patchEntity($user, $this->request->data);
                if ($this->Users->save($user)) {
                    $this->Flash->success(__('Guardado correctamente.'));

                    return $this->redirect(['action' => 'login']);
                }
                $this->Flash->error(__('Falla el registro.'));
            }
            $this->set(compact('user'));
            $this->set('_serialize', ['user']);
        }else {
            return $this->redirect(['action' => 'registro']);
        }

    }

    public function talentos (){
        $role =  $this->request->session()->read('Auth.User.role') ;
        $this->adminempresa($role);        
        #$users = $this->paginate($this->Users,[ 'conditions' => [ 'role' => 'user' ]] );
        #$users = $this->paginate($this->Users);
        #$users = $this->Users->find([ 'conditions' => [ 'role' => 'user' ]])->all();

        $users = $this->Users->find('all', [
          'conditions' => [ 'role' => 'user']
        ]);
        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    public function talentosbyall (){
        $role =  $this->request->session()->read('Auth.User.role') ;
        $this->adminempresa($role);
        if ($this->request->is('post')) { 

            $conditions['role'] = 'user'; 


            if (!empty($this->request->data['edad'])) {
                $conditions['universidad']= $this->request->data['universidad'];
            }

            if (!empty($this->request->data['edad'])) {
            // $conditions['edad'] = $Edad = $this->request->data['edad'];
            }
            if (!empty($this->request->data['estado'])) {
                $conditions['estado'] = $this->request->data['estado'];
            }
            if (!empty($this->request->data['carrera'])) {

                $conditions['carrera'] = $this->request->data['carrera'];
            }

            // Filtro de fecha created by Vikua

            if (!empty($this->request->data['desde']) && !empty($this->request->data['hasta'])) {

                $desde = date("Y-m-d", strtotime($this->request->data['desde']));
                $hasta = date("Y-m-d", strtotime($this->request->data['hasta']));
                $conditions[] = array(
                    "DATE(created) >=" => $desde,
                    "DATE(created) <=" => $hasta,
                );
            }

            $users = $this->Users->find('all', ['conditions' => [$conditions]]);


            //$this->paginate = ['limit' => 1000000000000000 ];
            //$users = $this->paginate($this->Users,[ 'conditions' => [ 'role' => 'user'  ,' YEAR( CURDATE( ) ) - YEAR(  `fecha_nacimiento` )= ' => $Edad,'Universidad' => $Universidad,'estado' => $Estado,'carrera' => $carrera ]] );
            //$users = $this->paginate($this->Users,[ 'conditions' => [ $conditions  ]] );

            $this->set(compact('users'));

        }else{
            $this->Flash->error(__('Disculpe pero los datos son incorrectos'));
        }
    }


    /* kpis */

    public function kpis(){
        $role =  $this->request->session()->read('Auth.User.role') ;
        $this->autorizadmin($role);

      $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://graph.facebook.com/?id=https://www.nestlecontigo.com.ve/iniciativaporlosjovenes/",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
             "Content-Type: application/json"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);


        $facebook = json_decode($response, true);


        $response=$facebook['share']['share_count'];

        $this->loadModel('kpis');

         $data = $this->kpis->get(1);
        
         

        $totalUsers = $this->Users->find('all', [
          'conditions' => [ 'role' => 'user']
        ]);

        $talentos = $totalUsers->count();


        $totalFinish = $this->Users->find('all', [
            'conditions' => [ 'role' => 'user', 'step' => '6']
        ]);

        $Finish = $totalFinish->count();

        for ($i=1; $i < 7; $i++) { 
           
            $steps = $this->Users->find('all', [
                'conditions' => [ 'role' => 'user', 'step' => $i]
            ]);
            $pasos[$i] = $steps->count();
        }


        $edad = $this->Users->find('all', [
            'conditions' => [ ' YEAR( CURDATE( ) ) - YEAR(  `fecha_nacimiento` ) < ' => '30']
        ]);

        $edadcount = $edad->count();

       $this->set('talentos',$talentos);
       $this->set('edadcount',$edadcount);
       $this->set('pasos',$pasos);
       $this->set('Finish',$Finish);
        $this->set('download', $data->dowload);
        $this->set('facebook',$response);



    }

   public function carga(){

    // Funcion de carga masiva by Vikua

        if ($this->request->is([ 'post'])) {
            $archivo = $this->request->data['archivo']['tmp_name'];
            $excelObject = \PhpExcel_IOFactory::load($archivo);
            $getSheet = $excelObject->getActiveSheet()->toArray(null);
            foreach ($getSheet as $data) {
                $user = $this->Users->newEntity();
                $this->request->data['first_name'] = $data[0];
                $this->request->data['last_name'] = $data[1];
                $this->request->data['universidad'] = $data[2];
                $this->request->data['estado'] = $data[3];
                $this->request->data['url_cv'] = '';
                $this->request->data['step'] = 0;
                $this->request->data['fecha_nacimiento'] = $data[4];
                $this->request->data['email'] = $data[5];
                $this->request->data['password'] = 0;
                $this->request->data['role'] = 'user';
                $this->request->data['active'] = 1;
                $this->request->data['created'] = date("Y-m-d H:i:s");
                $this->request->data['modified'] = date("Y-m-d H:i:s");
                $this->request->data['Username'] = '';
                $this->request->data['carrera'] = $data[6];
                $user = $this->Users->patchEntity($user, $this->request->data);
                if ($result=$this->Users->save($user)) {
                    


                }else{
                    $this->Flash->error(__('Hubo un error cargando el archivo, asegurese que los datos tengan la estructura correcta y no esten duplicados.'));
                    $this->Flash->default(__('Verifique la tabla. Si ya se cargaron datos, suba el archivo a partir del registro afectado.'));
                    return $this->redirect(['action' => 'talentos']);
                }
            }
            $this->Flash->success(__('Archivo cargado correctamente.'));
            return $this->redirect(['action' => 'talentos']);
        }
    }

    public function steps(){
         
       $this->autoRender = false;
        if ($this->request->is([ 'post'])) {
            
            $id =  $this->request->session()->read('Auth.User.id') ;
            $user = $this->Users->get($id, [
            'contain' => []
            ]);

            if ($user->step < $this->request->data['step']) {
                $user = $this->Users->patchEntity($user, $this->request->data);
                if ($this->Users->save($user)) {
                    $this->request->session()->write( 'Auth.User.step',$this->request->data['step']);
                  
                  echo "ok";

                }else{
                     echo "no";
                }
            }           
        }
    }


    public function forgotPassword($email = null){
        $this->viewBuilder()->layout( "LayoutLogin");
        if($this->request->is('post')) {
            $email = $this->request->data['email'];            
            $user = $this->Users->find()->where(['email' => $email ])->first();
            echo $user->first_name;
            if (!$user) {
                $this->Flash->error(__('Este usuario no existe'));
                return $this->redirect(['controller' => 'Users','action' => 'forgotPassword']);
            }else{                    
                $val = $this->generateRandomString();
                $this->request->data['password'] = $val;
                $user = $this->Users->patchEntity($user, $this->request->data);
                if ($this->Users->save($user)) {
                    $this->Flash->success(__('se envío un correo a su dirección '));
                    $email = new Email();
                    $email
                    ->template('default')
                    ->emailFormat('html')
                    ->to($user->email)
                    ->from('webadmin@nestle.com')
                    ->viewVars(['clave' => $val, 'user' => $user->Username, 'nombre' => $user->first_name])
                    ->send();

                    return $this->redirect(['action' => 'forgotPassword']);
                }
            }
        }
    }


    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }


    public function progresos()
    {
        $this->autoRender = false;
         if($this->request->is('post')) {
            
              $step = $this->request->data['step'];   
             $datas = $this->Users->find('all', [
                'conditions' => [ 'role' => 'user', 'step' => $step]
            ]);

             echo "<table class='table table-hover'>
                          <tr>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>Correo</th>
                            <th> Paso </th>
                          </tr>";
            foreach ($datas as $data ) {

                echo  "<tr>
                        <td>".$data->first_name."</td>
                        <td>".$data->last_name."</td>
                        <td>".$data->email."</td>
                        <td>".$data->step."</td>
                    </tr>";
                    }
                
        echo"</table>";
        }
    }


}


