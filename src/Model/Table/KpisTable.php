<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Kpis Model
 *
 * @method \App\Model\Entity\Kpi get($primaryKey, $options = [])
 * @method \App\Model\Entity\Kpi newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Kpi[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Kpi|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Kpi patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Kpi[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Kpi findOrCreate($search, callable $callback = null, $options = [])
 */
class KpisTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('kpis');
        $this->displayField('id');
        $this->primaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('dowload')
            ->requirePresence('dowload', 'create')
            ->notEmpty('dowload');

        return $validator;
    }
}
